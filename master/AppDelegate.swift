//
//  AppDelegate.swift
//  partizanMaster
//
//  Created by Иван Кулчеев on 11.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import CoreData
import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import GoogleMaps

let googleApiKey = "AIzaSyBBhsrf27tVcNU8cdLQXgP0543D5pShrY4"
let geocodingApiKey = "AIzaSyCT5aMIZxW2i19oK2D3VTekdAnxATzUCFU"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {

    var window: UIWindow?
    var exitViewController: UINavigationController?

    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
       // window?.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.262745098, alpha: 1)
        GMSServices.provideAPIKey(googleApiKey)
        FirebaseApp.configure()
        registerForPushNotifications(application)
        UINavigationBar.appearance().barStyle = .blackOpaque
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        let connectivity = Connectivity.shared
        connectivity.startNetworkReachabilityObserver()
        NSLog("[didFinishLaunchingWithOptions] successfully")
        return true
    }

    private func registerForPushNotifications(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // for iOS 10 and above
            //UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }

    // MARK: - Push notification registeration handler iOS 9 & iOS 10 (unchanged)
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Swift.Error) {
        print("Failed to register:", error)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        let notificationCenter = NotificationCenter.default
//        notificationCenter.addObserver(self,
//                                       selector: #selector(MapViewController.getDataFromRemotePush),
//                                       name: .dataDownloadCompleted,
//                                       object: nil)

        print("didReceiveRemoteNotification \(userInfo)")
       // fatalError("exit")
    }


    @available(iOS 10.0, *)
    private func processNotification(_ notif: UNNotification) {
        print(notif.request.content.userInfo)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        NSLog("[applicationWillResignActive] successfully")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        NSLog("[applicationDidEnterBackground] successfully")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NSLog("[applicationWillEnterForeground] successfully")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {

        NSLog("[applicationDidBecomeActive] successfully")

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        NSLog("[applicationWillTerminate] successfully")
        Connectivity.shared.startNetworkReachabilityObserver()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
//        self.saveContext()
    }

    // MARK: - Core Data stack

//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//         */
//        let container = NSPersistentContainer(name: "firebaseApp")
//        container.loadPersistentStores(completionHandler: { (_, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }

}
//@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//        NSLog("[UserNotificationCenter] applicationState:  didReceiveResponse: \(userInfo)")
//
//
////        guard let mapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapViewController") as? MapViewController else {return}
////        guard let navigationController = self.window?.rootViewController as? UINavigationController else {return}
////
////        navigationController.pushViewController(mapVC, animated: false)
//
//        completionHandler()
//    }
//}
