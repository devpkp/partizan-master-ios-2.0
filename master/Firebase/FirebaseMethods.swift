//
//  FirebaseMethods.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 16.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import CoreLocation

class FirebaseMethods {
    
    static let db = Firestore.firestore()
    
    static let messaging = Messaging.messaging()
    
    static func getUID() -> String {
        guard let userID = Auth.auth().currentUser?.uid else {
            print("[getUID] error get uid")
            return ""
        }
        return userID
    }
    
    static func authActions (error: Error?) {
        let userID = self.getUID()
        print(userID)
        messaging.subscribe(toTopic: userID)
        guard let deviceId = UIDevice.current.identifierForVendor?.uuidString else {
            print("[authActions] error getting deviceId")
            return
        }
        changeFieldsOfUser(["deviceId": deviceId])
        
        print("[authActions] done")
    }
    
    static func signOut(changeStatus: Bool) {
        do {
            let userID = self.getUID()
            messaging.unsubscribe(fromTopic: userID)
            if changeStatus {
                changeFieldsOfUser(["online": false])
            }
            try Auth.auth().signOut()
        } catch let err {
            print(err)
        }
        print("[signOut] done")
    }
    
    static func writeToUsers(user: User) {
        let userID = self.getUID()
        let docRefRegistration = FirebaseMethods.db.collection("users").document("\(userID)")
        docRefRegistration.setData(user.dictionary as [String : Any]) { (error) in
            if let error = error {
                print("[writeToUsers]" + error.localizedDescription)
                return
            } else {
                print("[writeToUsers] creating user in FB")
            }
        }
    }
    
    static func changeFieldsOfUser(_ dictionary: [String: Any]) {
        let userID = self.getUID()
        let docRefChangeStatus = FirebaseMethods.db.collection("users").document("\(userID)")
        docRefChangeStatus.updateData(dictionary) { err in
            if let err = err {
                print("[changeStatusOfUser] Error updating document: \(err)")
            } else {
                print("[changeStatusOfUser] Document successfully updated - \(dictionary)")
            }
        }
    }
    static func getStatusOfUser(finish: @escaping ((Bool?) -> Void)) {
        let userID = self.getUID()
        let docRefGetStatus = FirebaseMethods.db.collection("users").document("\(userID)")
        docRefGetStatus.getDocument { (document, _) in
            if let document = document, document.exists {
                guard let dataDescription = document.data() else { return }
                print("[getStatusOfUser] Document data: \(String(describing: dataDescription))")
                let status = dataDescription["online"] as? Bool
                finish(status)
                
            } else {
                print("[getStatusOfUser] Document does not exist")
            }
        }
    }
    
    static func writeTimerWasLaunchedOToOrder(documentId: String, finish: @escaping ((Date) -> Void)) {
        let docRefRegistration = FirebaseMethods.db.collection("orders").document("\(documentId)")
        let currentDate = Date(timeIntervalSinceNow: -10)
        docRefRegistration.updateData(["timerWasLaunched": currentDate]) { (error) in
            if let error = error {
                print("[writeTimerWasLaunchedOToOrder]" + error.localizedDescription)
                return
            } else {
                print("[writeTimerWasLaunchedOToOrder] updating order date in FB")
                finish(currentDate)
            }
        }
    }
    static func getStatisticOfUse(finish: @escaping ((Statistic) -> Void)) {
        let userID = self.getUID()
        let docRefGetStatus = FirebaseMethods.db.collection("userStats").document("\(userID)")
        docRefGetStatus.getDocument { (document, _) in
            if let document = document, document.exists {
                guard let dataDescription = document.data() else { return }
                print("[getStatisticOfUser] Document data: \(String(describing: dataDescription))")
                let finalStatistic = Statistic(statisticDictionary: dataDescription)
                finish(finalStatistic)
            } else {
                print("[getStatusOfUser] Document does not exist")
            }
        }
    }
    
    // MARK: new methods
    static func generateReportId() -> String {
        let docRefSendReport = FirebaseMethods.db.collection("reports").document()
        let id = docRefSendReport.documentID
        return id
    }
    static func writeToReports(id: String, report: Report, finish: @escaping ((String) -> Void)) {
        let docRefSendReport = FirebaseMethods.db.collection("reports").document(id)
        docRefSendReport.setData(report.dictionary as [String : Any]) { (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            } else {
                print("[writeToReports] Order in firebase")
                finish(docRefSendReport.documentID)
            }
            
        }
    }
    static func getDocumentsByUID(finish: @escaping (([Order]) -> Void)) {
        let userID = self.getUID()
        let collectionRefOrders = FirebaseMethods.db.collection("orders").whereField("userId", isEqualTo: userID)
        collectionRefOrders.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                print("Error fetching snapshot results: \(error?.localizedDescription)")
                return
            }
            let models = snapshot.documents.compactMap {
                return Order(dictionary: $0.data(), id: $0.documentID)
            }
            let modelExceptModernization = models.filter { $0.status != .modernization }
            finish(modelExceptModernization)
        }
    }
    
    static func getDocumentsModernization(finish: @escaping (([Order]) -> Void)) {
        let userID = self.getUID()
        let collectionRefOrders = FirebaseMethods.db.collection("orders").whereField("userId", isEqualTo: userID)//.whereField("status", isEqualTo: "modernization")
        collectionRefOrders.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                print("Error fetching snapshot results: \(error?.localizedDescription)")
                return
            }
            let models = snapshot.documents.compactMap {
                return Order(dictionary: $0.data(), id: $0.documentID)
                }.filter { $0.status == .modernization || $0.status == .processing }
            finish(models)
        }
    }
    
    static func updateOrder(dictionary: [String: Any], documentId: String) {
        let collectionRefOrdersForUpdate = FirebaseMethods.db.collection("orders").document(documentId)
        collectionRefOrdersForUpdate.updateData(dictionary) { err in
            if let err = err {
                print("[updateStatusOfOrder] Error updating document: \(err)")
            } else {
                print("[updateStatusOfOrder] Document successfully updated - \(dictionary)")
            }
        }
        
    }
    static func getUserStats(finish: @escaping (([UserStats]) -> Void)) {
        let userID = self.getUID()
        let collectionRefReports = FirebaseMethods.db.collection("userStats").document(userID)
        collectionRefReports.getDocument { (document, _) in
            if let document = document, document.exists {
                guard let dataDescription = document.data() else { return }
                print("[getUserStats] Document data: \(String(describing: dataDescription))")
                guard let model = UserStats(dictionary: dataDescription) else { return }
                var arr: [UserStats] = []
                arr.append(model)
                finish(arr)
                
            } else {
                print("[getStatusOfUser] Document does not exist")
            }
        }
        
    }
    
    static func getReports(finish: @escaping (([Report]) -> Void)) {
        let userID = self.getUID()
        let collectionRefReports = FirebaseMethods.db.collection("reports").whereField("userId", isEqualTo: userID)
        collectionRefReports.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                print("Error fetching snapshot results: \(error?.localizedDescription)")
                return
            }
            let models = snapshot.documents.compactMap {
                return Report(dictionary: $0.data())
            }
            finish(models)
        }
    }
    
    static func dialRequest(id: String) {
        let docRefRegistration = FirebaseMethods.db.collection("dialRequests").document(id)
        docRefRegistration.setData(["call": true]) { (error) in
            if let error = error {
                print("[dialRequest]" + error.localizedDescription)
                return
            } else {
                print("[dialRequest] request to call")
            }
        }
    }
    static func getHistoryReports(historyReports: String, finish: @escaping ((HistoryReports) -> Void)) {
        let collectionRefReports = FirebaseMethods.db.collection("reports").document(historyReports)
        collectionRefReports.getDocument { (document, _) in
            if let document = document, document.exists {
                guard let dataDescription = document.data() else { return }
                print("[getHistoryReports] Document data: \(String(describing: dataDescription))")
                guard let model = HistoryReports(dictionary: dataDescription) else { return }
                
                finish(model)
                
            } else {
                print("[getHistoryReports] Document does not exist")
            }
        }
    }
    
    
    
    static func getCurrentMonthReports(finish: @escaping (([Report]) -> Void)) {
        let userID = self.getUID()
        let collectionRefReports = FirebaseMethods.db.collection("reports").whereField("userId", isEqualTo: userID).whereField("date", isGreaterThanOrEqualTo: Date().startOfMonth())
        collectionRefReports.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                print("Error fetching snapshot results: \(error!)")
                return
            }
            let models = snapshot.documents.compactMap {
                return Report(dictionary: $0.data())
            }
            finish(models)
        }
    }
    static func getPreviousMonthReports(finish: @escaping (([Report]) -> Void)) {
        let userID = self.getUID()
        let collectionRefReports = FirebaseMethods.db.collection("reports").whereField("userId", isEqualTo: userID).whereField("date", isLessThanOrEqualTo: Date().startOfMonth())
        collectionRefReports.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                print("Error fetching snapshot results: \(error!)")
                return
            }
            let models = snapshot.documents.compactMap {
                return Report(dictionary: $0.data())
            }
            finish(models)
        }
    }
    
    static func batchProcessingWrites(status: String, documentId: String) {
        let batch = db.batch()
        
        let collectionRefOrdersForUpdate = FirebaseMethods.db.collection("orders").document(documentId)
        batch.updateData(["status": status], forDocument: collectionRefOrdersForUpdate)
        
        let currentDate = Date(timeIntervalSinceNow: -10)
        batch.updateData(["timerWasLaunched": currentDate], forDocument: collectionRefOrdersForUpdate)
        
        batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                print("Batch write succeeded.")
            }
        }
    }
    
    
    static func checkValidOrder(_ documentId: String, finish: @escaping ((Bool) -> Void))  {
        let collectionRefOrdersForUpdate = FirebaseMethods.db.collection("orders").document(documentId)
        collectionRefOrdersForUpdate.getDocument(source: .server) { (document, _) in
            guard let document = document,
                let documentData = document.data(),
                let _ = Order(dictionary: documentData, id: document.documentID) else {
                    print("[checkValidOrder] no valid order")
                    finish(false)
                    return
            }
            finish(true)
        }
    }
    static func updateLocationUser(event: String?, location: CLLocation?, orderId: String?, userId: String?) {
        let collectionRefLocationsForUpdate = FirebaseMethods.db.collection("userLocations").document()
        let locationGeoPoint = GeoPoint(latitude: location?.coordinate.latitude ?? 0, longitude: location?.coordinate.longitude ?? 0)
        let dictionary = [
            "event":event,
            "location":locationGeoPoint,
            "orderId":orderId,
            "userId":userId
            ] as [String : Any?]
        collectionRefLocationsForUpdate.setData(dictionary as [String : Any]){ err in
            if let err = err {
                print("[updateStatusOfOrder] Error updating document: \(err)")
            } else {
                print("[updateStatusOfOrder] Document successfully updated - \(dictionary)")
            }
        }
    }
    static func checkVersion(finish: @escaping ((Bool) -> Void))  {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String else {
                return
        }
        let collectionRefOrdersForUpdate = FirebaseMethods.db.collection("versions").document("ios")
        collectionRefOrdersForUpdate.getDocument(source: .server) { (document, _) in
            guard let document = document,
                let documentData = document.data(),
                let number = documentData["number"] as? String,
                let devNumber = documentData["devNumber"] as? String else {
                    print("[checkVersion] failed to compare version")
                    finish(false)
                    return
            } 
            (currentVersion == number || currentVersion == devNumber) ? finish(true) : finish(false)
        }
    }
}

