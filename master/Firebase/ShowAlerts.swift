//
//  ShowAlerts.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 16.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import Foundation

class ShowAlerts {
    func showAlert(viewControllerToShow: UIViewController, headerTitle: String, message: String, okTitle: String) {
        let alert = UIAlertController(title: headerTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: okTitle, style: UIAlertActionStyle.default, handler: nil))
        viewControllerToShow.present(alert, animated: true, completion: nil)
    }
}
