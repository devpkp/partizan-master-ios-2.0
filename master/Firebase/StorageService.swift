//
//  StorageService.swift
//  master
//
//  Created by Sergey Gusev on 19.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Firebase

struct StorageService {

    static func uploadImage(_ image: UIImage, at reference: StorageReference, completion: @escaping (URL?) -> Void) {
        guard let imageData = image.jpegData(compressionQuality: 1) else {
            return completion(nil)
        }
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        reference.putData(imageData, metadata: metadata, completion: {  (_, error) in
            if let error = error {
                /*assertionFailure*/ print(error.localizedDescription)
                return completion(nil)
            }
            reference.downloadURL(completion: { (url, error) in
                if let error = error {
                    /*assertionFailure*/ print(error.localizedDescription)
                    return completion(nil)
                }
                completion(url)
            })
        })
    }
}
