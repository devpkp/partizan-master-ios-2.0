//
//  PostService.swift
//  master
//
//  Created by Sergey Gusev on 19.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import Firebase

struct ImageToSend {
    let image: UIImage
    let name: String
}

struct PostService {
    static func create(for imagesNames: [String], progressView: UIProgressView, closure: @escaping (Bool) -> Void) {
        let dispatchGroup = DispatchGroup()
        let countImages = imagesNames.count
        var i = 0
        for item in imagesNames {
            dispatchGroup.enter()
            let imageRef = Storage.storage().reference().child("ReportPhotos/\(item).jpg")
            guard let image = ImageStore.retrieve(imageNamed: item) else {return}
            StorageService.uploadImage(image, at: imageRef) { (downloadURL) in
                guard let downloadURL = downloadURL else {
                    dispatchGroup.leave()
                    return closure(false)
                }
                i += 1
                progressView.setProgress(Float(i / countImages), animated: true)
                let urlString = downloadURL.absoluteString
                print("image url: \(urlString)")
                dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            print("all photos uploaded 👍")
            closure(true)
        }
    }
}
