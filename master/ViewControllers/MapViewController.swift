//
//  MapViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 13.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import UserNotifications
import SideMenu
import FirebaseMessaging
import Firebase

var canChangeOnlineStatus = true

var activeRequestCount = 0

class MapViewController: SuperMenuViewController {
    typealias documentDictionary = [String : Any]
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var popUpButton: UIButton!
    @IBOutlet weak var requestState: UIButton!
    
    @IBOutlet weak var nameOfCustomerLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var startJobButton: UIButton!
    @IBOutlet weak var refuseRequestButton: UIButton!
    @IBOutlet weak var requestCompleteButton: UIButton!
    
    @IBOutlet weak var problemWithPOLabel: UILabel!
    @IBOutlet weak var problemSdLabel: UILabel!
    @IBOutlet weak var historyView: UIView!
    var blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var historyTableView: UITableView!
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.text = "История пуста"
        label.textColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.2588235294, alpha: 1)
        label.textAlignment = .center
        return label
    }()
    
    var sizeOfPopup: CGFloat {
        return popUpView.frame.height - popUpButton.frame.height - requestState.frame.height - 8
    }
    
    
    var isTimerRunning = false
    var popUpViewIsOpen = false
    var orders: [Order] = [] {
        didSet {
            canChangeOnlineStatus = orders.isEmpty
        }
    }
    var timer = Timer()
    let locationManager = CLLocationManager()
    var timeForTimer = 0
    
    var phoneButton: UIBarButtonItem?
    var myLocation: CLLocation?
    
    var activeOrder: Order?
    var historyDictionary: [Int: HistoryReports] = [:]
    private var listener: ListenerRegistration?
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    lazy var completeView: UIView = {
        let completeView = UIView(frame: CGRect(x: 8, y: -100, width: view.frame.size.width - 16, height: 70))
        completeView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        completeView.layer.masksToBounds = true
        completeView.layer.cornerRadius = 10.0
        completeView.alpha = 0.8
        return completeView
    }()
    lazy var labelCompleteView: UILabel = {
        let labeld = UILabel()
        labeld.text = "Отчет успешно отправлен"
        labeld.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labeld.frame = completeView.frame
        labeld.textAlignment = .center
        return labeld
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Карта заявок" 
        initView()
        UNUserNotificationCenter.current().delegate = self
        
        initObservers()
        addTargets()
        mapInit()
        listenerOrders()
        addPhone()
        popUpView.isUserInteractionEnabled = false
        checkInternet()
        checkVersion()
      
    }
    func checkVersion() {
        FirebaseMethods.checkVersion() { [weak self] check in
            if !check {
                DispatchQueue.main.async {
                    
                    let noInternetViewController = UpdatingAppViewController(nibName: "UpdatingAppViewController", bundle: nil)
                    self?.present(noInternetViewController, animated: true)
//                    ShowAlerts.showAlertAction(viewControllerToShow: noInternetViewController, headerTitle: "Вышла более новая версия", message: "Обновите приложение в App Store", okTitle: "Открыть App Store", closure: {
//                        if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(id)"),
//                            UIApplication.shared.canOpenURL(url){
//                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                        }
//                    })
                    //                        let alert = UIAlertController(title: "Вышла более новая версия", message: "Обновите приложение в AppStore", preferredStyle: UIAlertController.Style.alert)
                    //
                    //                        self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    func checkInternet() {
        let connectivity = Connectivity.shared
        if !Connectivity.isConnectedToInternet {
            connectivity.pushNoInternetController()
        }
    }
    func listenerOrders() {
        let userID = FirebaseMethods.getUID()
        let queryListenerOrders = FirebaseMethods.db.collection("orders").whereField("userId", isEqualTo: userID)
        listener = queryListenerOrders.addSnapshotListener { [weak self] documentSnapshot, error in
            documentSnapshot?.documentChanges.forEach { diff in
                if (diff.type == .added) {
                    self?.historyDictionary.removeAll()
                }
            }
            guard let documents = documentSnapshot?.documents else {
                print("Error fetching document: \(error!)")
                return
            }
            let downloadedOrders = documents.compactMap {
                return Order(dictionary: $0.data(), id: $0.documentID)
            }
            activeRequestCount = downloadedOrders.filter { $0.status == .processing || $0.status == .modernization }.count
            NotificationCenter.default.post(name: .changeRequest, object: nil)
            
            self?.orders = downloadedOrders.filter { $0.status != .modernization && $0.status != .awaiting }
            self?.orders.sort { $0.expireTime ?? Date() < $1.expireTime ?? Date() }
            self?.checkStatusOfOrder()
        }
        
    }
    func initObservers() {
        print("Oserver")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadTimer),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    @objc func loadTimer() {
        self.checkStatusOfOrder()
        
    }
    func addPhone() {
        phoneButton = UIBarButtonItem(image: #imageLiteral(resourceName: "telephone"), style: .plain, target: self, action: #selector(makeCall))
    }
    @objc func makeCall() {
        guard let id = activeOrder?.id else {return}
        FirebaseMethods.dialRequest(id: id)
        ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: "Вам сейчас перезвонят", message: "", okTitle: Constants.okString)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        locationManager.startUpdatingLocation()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        commentTextView.setContentOffset(.zero, animated: false)
    }
    
    
    
    
    
    @IBAction func upButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4) {
            if !self.popUpViewIsOpen {
                self.popUpView.frame.origin.y -= self.sizeOfPopup
                self.requestState.setTitle("", for: .normal)
                self.historyButton.isHidden = false
                self.moveGeoPoint(isOpen: true)
                
            } else {
                self.popUpView.frame.origin.y += self.sizeOfPopup
                self.requestState.setTitle("Активная заявка", for: .normal)
                self.historyButton.isHidden = true
                self.moveGeoPoint(isOpen: false)
            }
        }
        popUpViewIsOpen = !popUpViewIsOpen
    }
    
    
    
    deinit {
        print("Deinit MapViewController")
        NotificationCenter.default.removeObserver(self,name: UIApplication.willEnterForegroundNotification, object: nil)
        listener?.remove()
    }
    
}

