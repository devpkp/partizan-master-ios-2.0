//
//  ReportListViewController.swift
//  master
//
//  Created by Polina Guryeva on 28/09/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class ReportListViewController: SuperMenuViewController {
    
    var popupViewController: SBCardPopupViewController?
    
    @IBOutlet var filterView: UIView!
    var filterBarButton: UIBarButtonItem!
    
    @IBOutlet weak var reportsTableView: UITableView!
     @IBOutlet var fullReportView: UIView!
    
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var commentOfMasterLabel: UILabel!
    @IBOutlet weak var commentOfOrderLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var numberOfReportLabel: UILabel!
    
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var fromPriceTextField: UITextField!
    @IBOutlet weak var toPriceTextField: UITextField!
    
    var refreshControl: UIRefreshControl!
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    var choosenSegment = 0
    
    lazy var emptyTableLabel: UILabel = {
        let label = UILabel()
        label.text = "Список отчетов пуст"
        label.textColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.2588235294, alpha: 1)
        label.textAlignment = .center
        return label
    }()
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    let states = ["Любой", "Выполненные", "Сложная диагностика", "Отмененные", "Отказы", "Модернизация"]
    let stateDictionary: [String: String] = ["Любой": "any", "Выполненные": "completed", "Сложная диагностика": "sd", "Отмененные": "canceled", "Отказы": "declined", "Модернизация": "modernization"]
    let stateDictionaryReverse: [String: String] = ["any": "Любой", "completed": "Выполненные", "sd": "Сложная диагностика", "canceled": "Отмененные", "declined": "Отказы", "modernization": "Модернизация"]
    
    var pickerView = UIPickerView()
    let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    var reports: [Report] = []
    let stateIcons: [StatusOfReport: UIImage] = [.completed: #imageLiteral(resourceName: "ok"), .sd: #imageLiteral(resourceName: "eye"), .canceled: #imageLiteral(resourceName: "cancel"), .declined: #imageLiteral(resourceName: "refuse"), .modernization: #imageLiteral(resourceName: "modernization")]
    
    func actionSegment(action: @escaping () -> Void) {
        switch choosenSegment  {
        case 0:
            getCurrentReports(closure: action)
        case 1:
            getPreviousReports(closure: action)
        default:
            return
        }
    }
    
    @objc func handleRefresh() {
        actionSegment {
            self.refreshControl.endRefreshing()
            self.filterReports()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.dataSource = self
        pickerView.delegate = self
        statusTextField.inputView = pickerView
        reportsTableView.dataSource = self
        reportsTableView.delegate = self
        
        viewInit()
        addActivityIndicator()
        getCurrentReports() {
            self.removeActivityIndicator()
        }
    }
    func addActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
    }
    
    func removeActivityIndicator() {
        activityIndicator.removeFromSuperview()
    }
    func viewInit() {
        reportsTableView.rowHeight = UITableView.automaticDimension
        reportsTableView.separatorStyle = .none
        
        filterBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "filter"), style: .plain, target: self, action: #selector(setFilters))
        navigationItem.rightBarButtonItem = filterBarButton
        
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.262745098, alpha: 1)
        refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        reportsTableView.addSubview(refreshControl)
    }
    
    func getCurrentReports(closure: @escaping () -> Void) {
        FirebaseMethods.getCurrentMonthReports { (documents) in
            self.reports.removeAll()
            for document in documents {
                self.reports.append(document)
            }
            self.reports.sort { $0.date ?? Date() > $1.date ?? Date() }
            self.reportsTableView.backgroundView = self.reports.isEmpty ? self.emptyTableLabel : nil
            self.reportsTableView.reloadSections([0], with: .fade)
            closure()
        }
    }
    func getPreviousReports(closure: @escaping () -> Void) {
        FirebaseMethods.getPreviousMonthReports { (documents) in
            self.reports.removeAll()
            for document in documents {
                self.reports.append(document)
            }
            self.reports.sort { $0.date ?? Date() > $1.date ?? Date() }
            self.reportsTableView.backgroundView = self.reports.isEmpty ? self.emptyTableLabel : nil
            self.reportsTableView.reloadSections([0], with: .fade)
            closure()
        }
    }
  
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        choosenSegment = sender.selectedSegmentIndex
        actionSegment {
            self.filterReports()
        }
    }
    
    @objc func setFilters() {
        filterView.frame.size.width = view.bounds.size.width * 0.9
        filterView.center = view.center
        popupViewController = SBCardPopupViewController(contentView: filterView)
        popupViewController?.show(onViewController: self)
    }
    
    func filterReports() {
        if let state = statusTextField.text, !state.isEmpty, stateDictionary[state] != "any", let statusOfFilter = StatusOfReport( rawValue: stateDictionary[state] ?? "") {
            reports = reports.filter { $0.status == statusOfFilter }
        }
        if let fromPrice = fromPriceTextField.text, !fromPrice.isEmpty, let doubleFromPrice = Double(fromPrice),
           let toPrice = toPriceTextField.text, !toPrice.isEmpty, let doubleToPrice = Double(toPrice) {
            print(doubleFromPrice)
            print(doubleToPrice)
            reports = reports.filter { Double($0.cost ?? 0.0) >= doubleFromPrice && Double($0.cost ?? 0.0) < doubleToPrice}
        }
        
        reportsTableView.backgroundView = reports.isEmpty ? emptyTableLabel : nil
        self.reportsTableView.reloadSections([0], with: .fade)
    }
    
    @IBAction func showFiltredReports(_ sender: UIButton) {
        popupViewController?.view.endEditing(true)
        actionSegment {
            self.filterReports()
        }
        popupViewController?.close()
    }
    
    @IBAction func cancelFilteringReports(_ sender: Any) {
        popupViewController?.close()
    }
    func clearFilterFields() {
        statusTextField.text = ""
        fromPriceTextField.text = ""
        toPriceTextField.text = ""
        cancelFilteringReports(UIButton())
    }
    @IBAction func discardAllFilters(_ sender: Any) {
        actionSegment {
            self.clearFilterFields()
        }
    }
    
    @IBAction func closeFullReportViewButtonTapped(_ sender: UIButton) {
        self.popupViewController?.close()
    }
    
}

// MARK: - picker view data source
extension ReportListViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
}

// MARK: - picker view delegate
extension ReportListViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        statusTextField.text = states[row]
        popupViewController?.view.endEditing(true)
    }
}

// MARK: - table view data source
extension ReportListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTableViewCell") as? ReportTableViewCell else {return UITableViewCell()}
        cell.numberLable.text = "Отчет \(String(describing: reports[indexPath.row].orderNumber ?? "")  )"
        cell.dateLabel.text = dateFormatter.string(from: reports[indexPath.row].date ?? Date())
        cell.stateIcon.image = stateIcons[reports[indexPath.row].status ?? .def]
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = #colorLiteral(red: 0.8839493642, green: 0.8839493642, blue: 0.8839493642, alpha: 1)
        }
        return cell
    }
}

// MARK: - table view delegate
extension ReportListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        sumLabel.text = "\(String(describing: reports[indexPath.row].cost ?? 0))"
        timeLabel.text =  "\(String(describing: (reports[indexPath.row].timeSpend  ?? 60) / 60)) мин."
        statusIcon.image = stateIcons[reports[indexPath.row].status ?? .def]
        statusLabel.text = stateDictionaryReverse[reports[indexPath.row].status?.rawValue ?? "def"]
        let repairs = reports[indexPath.row].usedDetails?.reduce("") { $0 + $1.list }.dropLast()
        detailsLabel.text = String(repairs == "" ? "Нет деталей" : repairs ?? "Нет деталей")
        commentOfMasterLabel.text = reports[indexPath.row].comments
        commentOfOrderLabel.text = reports[indexPath.row].orderDescription
        adressLabel.text = reports[indexPath.row].orderAddress?.fullAddressDescriptor
        numberOfReportLabel.text = "Отчет \(String(describing: reports[indexPath.row].orderNumber ?? "")  )"
        
        fullReportView.frame.size.width = tableView.bounds.size.width * 0.9
        fullReportView.center = view.center
        popupViewController = SBCardPopupViewController(contentView: fullReportView)
        popupViewController?.show(onViewController: self)
    }
}

