//
//  ReportsViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 16.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import SideMenu

// TODO: rename
class ReportsViewController: SuperMenuViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var labels = ["Количество заявок", "Выполненные", "На диагностике", "Отмененные", "Отказы", "Процент отказов", "Процент диагностик", "Сумма по заявкам", "К получению", "Получено по факту"]
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var requestCountLabel = ""
    var completeCountLabel = ""
    var sdCountLabel = ""
    var cancelCountLabel = ""
    var refuseCountLabel = ""
    var percentOfRefuseLabel = ""
    var percentOfSdLabel = ""
    var currMonthProfitLabel = ""
    var prevMonthProfitLabel = ""

    var refreshControl: UIRefreshControl!
    
    var userStat: [UserStats] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        viewInit()
        getUserStat (closure: {
            self.reloadTableView()
        })
    }
    func reloadTableView() {
        tableView.reloadSections([0], with: .fade)
        
//        UIView.transition(with: tableView, duration: 0.5, options: .transitionCrossDissolve, animations: {
//            self.tableView.reloadData()
//        }, completion: nil)
    }
    func getUserStat(closure: @escaping () -> Void) {
        FirebaseMethods.getUserStats { (documents) in
            self.userStat = documents
            switch self.segmentedControl.selectedSegmentIndex{
            case 0:
                self.fillLabels(stat: self.userStat.first?.currentMonthStats)
            case 1:
                self.fillLabels(stat: self.userStat.first?.previousMonthStats)
            default:
                break
            }
            closure()
        }
    }
    
    @IBAction func monthChanged(_ sender: Any) {
        getUserStat (closure: {
            self.reloadTableView()
        })
    }
    
    @objc func handleRefresh() {
        getUserStat {
            self.refreshControl.endRefreshing()
        }
    }
    
    func viewInit() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false

        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = #colorLiteral(red: 0.9475002842, green: 0.9475002842, blue: 0.9475002842, alpha: 1)
        refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    func fillLabels(stat: MonthStats?) {
        
        //let counts = reports.reduce(into: [:]) { counts, word in counts[word.status ?? .def, default: 0] += 1 }
        //let allCash = reports.reduce(0.0, {$0 + ($1.cost ?? 0.0)})
        self.requestCountLabel = String(describing: stat?.total ?? 0)
        self.completeCountLabel = String(describing: stat?.completed ?? 0)
        self.sdCountLabel = String(describing: stat?.sd ?? 0)
        self.cancelCountLabel = String(describing: stat?.canceled ?? 0)
        self.refuseCountLabel = String(describing: stat?.declined ?? 0)
        self.currMonthProfitLabel = String(describing: stat?.earned ?? 0.0)
        
        if stat?.total == 0 {
            self.percentOfRefuseLabel = "-"
            self.percentOfSdLabel = "-"
        } else {
            self.percentOfRefuseLabel = "\(String(describing: stat?.declinedPercent ?? 0))%"
            self.percentOfSdLabel = "\(String(describing: stat?.sdPercent ?? 0))%"
        }
        self.reloadTableView()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)

    }
    deinit {
        print("Deinit ReportsViewController")
    }
   
}

extension ReportsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "statisticCell") as? StatTableViewCell else {return UITableViewCell()}
        cell.label.text = labels[indexPath.row]
        
        // TODO: case 7,8,9 need to change field text
        
        switch indexPath.row {
        case 0:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: requestCountLabel, lineIsVisible: true, textSize: 20)
        case 1:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: completeCountLabel, lineIsVisible: true, textSize: nil)
        case 2:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: sdCountLabel, lineIsVisible: true, textSize: nil)
        case 3:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: cancelCountLabel, lineIsVisible: true, textSize: nil)
        case 4:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: refuseCountLabel, lineIsVisible: true, textSize: nil)
        case 5:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: percentOfSdLabel, lineIsVisible: true, textSize: nil)
        case 6:
            cell.initStatCell(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), countTextColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), text: percentOfRefuseLabel, lineIsVisible: true, textSize: nil)
        case 7:
            cell.initStatCell(textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), countTextColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), text: currMonthProfitLabel, lineIsVisible: false, textSize: nil)
        case 8:
            cell.initStatCell(textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), countTextColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), text: "Недоступно", lineIsVisible: false, textSize: nil)
        case 9:
            cell.initStatCell(textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), countTextColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), text: "Недоступно", lineIsVisible: false, textSize: nil)
        default:
            break
        }
        return cell
    }
}
