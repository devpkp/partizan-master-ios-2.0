//
//  SettingsViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 16.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import SideMenu

class SettingsViewController: SuperMenuViewController {

    let sections = ["Личные данные", "Звуковые настройки", "Информация"]
    @IBOutlet weak var settingsTableView: UITableView!

    let infoLabels = ["О приложении", "Лицензия"]
    let infoText = ["Мы обрабатываем и выполняем заявки круглосуточно Как мы это успеваем делать? - мы уже 10 лет оказываем услуги по ремонту и настройке техники в России - у нас собственные сервисные центры в 40 городах на территории РФ - у нас собственный огромный callcenter - у нас в штате более 650 выездных сервис- инженеров, прошедших обучение на базе собственного учебного центра - мы работаем круглосуточно и без выходных - и да, мы - крупнейшая компания по выездному ремонту техники в стране.",
                    "Пользуясь сервисами Google, Вы доверяете нам свою личную информацию. Мы делаем все для обеспечения ее безопасности и в то же время предоставляем Вам возможность управлять своими данными."]

    override func viewDidLoad() {
        super.viewDidLoad()
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        viewInit()
    }

    func viewInit() {
        navigationItem.title = "Настройки"
        settingsTableView.separatorStyle = .none
        settingsTableView.rowHeight = UITableView.automaticDimension
        settingsTableView.estimatedRowHeight = 20
        settingsTableView.allowsSelection = false
    }

    deinit {
        print("Deinit SettingsViewController")
    }
}

extension SettingsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 2
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "personalDataTableViewCell") as? PersonalDataTableViewCell else {return UITableViewCell()}
            cell.secondName.text = user?.lastName ?? "Фамилия"
            cell.firstName.text = user?.firstName ?? "Имя"
            cell.patronymic.text = user?.middleName ?? "Отчество"
            cell.id.text = ""
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "personalSettingTableViewCell") as? PersonalSettingTableViewCell else {return UITableViewCell()}
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "infoTableViewCell") as? InfoTableViewCell else {return UITableViewCell()}
            cell.infoLabel.text = infoLabels[indexPath.row]
            cell.infoText.text = infoText[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
}

extension SettingsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 25))
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.size.width, height: 30))
        label.text = sections[section]
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.262745098, alpha: 1)
        headerView.addSubview(label)
        headerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 20))
        footerView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return footerView
    }

}
