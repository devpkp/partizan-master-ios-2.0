//
//  ActiveViewController.swift
//  master
//
//  Created by Polina Guryeva on 03.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import Foundation

class ActiveViewController: SuperMenuViewController {

    @IBOutlet weak var activeRequestsTableView: UITableView!
    var activeRequests: [Order] = []

    var refreshControl: UIRefreshControl!
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.text = "Нет активных заявок"
        label.textColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.2588235294, alpha: 1)
        label.textAlignment = .center
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        activeRequestsTableView.delegate = self
        activeRequestsTableView.dataSource = self
        activeRequestsTableView.separatorStyle = .none
        activeRequestsTableView.rowHeight = UITableView.automaticDimension
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = #colorLiteral(red: 0.1764705882, green: 0.2, blue: 0.262745098, alpha: 1)
        refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        activeRequestsTableView.addSubview(refreshControl)
        
//        addActivityIndicator()
//        getOrders {
//            self.removeActivityIndicator()
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addActivityIndicator()
        getOrders {
            self.removeActivityIndicator()
        }
    }
    @objc func handleRefresh() {
        getOrders {
            self.refreshControl.endRefreshing()
        }
    }
    func getOrders(closure: @escaping () -> Void) {
        FirebaseMethods.getDocumentsModernization { documents in
            self.activeRequests.removeAll()
            self.activeRequests = documents

            self.activeRequests.sort { $0.startWorkTime ?? Date() > $1.startWorkTime ?? Date() }
            self.activeRequestsTableView.backgroundView = self.activeRequests.isEmpty ? self.label : nil
            self.activeRequestsTableView.reloadData()
            closure()
        }
    }
    func addActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
    }
    
    func removeActivityIndicator() {
        activityIndicator.removeFromSuperview()
    }
    @objc func sendReportButtonTapped(sender:UIButton) {
        
        guard let onlineStatus = user?.online,
            onlineStatus == true else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.attensionAlertMessage, message: Constants.checkOnlineStatus, okTitle: Constants.okString)
                return
        }
        
        
        let buttonRow = sender.tag
        guard let sendReportVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sendReportViewCotroller") as? SendReportViewController else {return}
        guard let navigationController = self.navigationController else {return}

        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        navigationController.view.layer.add(transition, forKey: nil)
        guard let id = self.activeRequests[buttonRow].id,
            let number = self.activeRequests[buttonRow].number,
            let description = self.activeRequests[buttonRow].description,
            let address = self.activeRequests[buttonRow].address,
            let clientName = self.activeRequests[buttonRow].clientName,
            let timerWasLaunched = self.activeRequests[buttonRow].timerWasLaunched,
            let userName = user?.firstName else {
                print("Failure")
                return
        }
        let timeSpend = Date() - timerWasLaunched
        sendReportVC.commonInit(orderId: id, orderNumber: number, orderDescription: description, orderAddress: address, timeOfWork: Int(timeSpend), clientName: clientName, userName: userName)
        navigationController.pushViewController(sendReportVC, animated: false)
    }
    deinit {
        print("[ActiveViewController] deinit ")
    }

}

extension ActiveViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activeRequests.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "activeRequestCell") as? ActiveRequestTableViewCell else {return UITableViewCell()}
        cell.adress.text = activeRequests[indexPath.row].address?.fullAddressDescriptor
        cell.commentTextView.text = activeRequests[indexPath.row].description
        cell.nameOfClient.text = activeRequests[indexPath.row].clientName
        cell.sendReportButton.tag = indexPath.row
        cell.sendReportButton.addTarget(self, action: #selector(sendReportButtonTapped), for: .touchUpInside)
        cell.isModernizationLabel.isHidden = !(activeRequests[indexPath.row].status == .modernization)
        return cell
    }
}

extension ActiveViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
