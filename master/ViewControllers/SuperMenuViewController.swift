//
//  SuperMenuViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 17.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import SideMenu

class SuperMenuViewController: UIViewController {

    var menuIsOpen = false

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.tintColor = .white

        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "hamburger"), style: .plain, target: self, action: #selector(hamurgerButtonTapped))
        navigationItem.leftBarButtonItem = barButton

        configureSideMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc func hamurgerButtonTapped() {
        guard let menuLeftNavigationController = SideMenuManager.default.menuLeftNavigationController else {return}
        present(menuLeftNavigationController, animated: true, completion: nil)
    }

    func configureSideMenu () {
        guard let menuLeftNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuViewController") as? UISideMenuNavigationController else {return}
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController

//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu: .left)
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width * 0.3
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.clear
        SideMenuManager.default.menuShadowOpacity = 0.5
        SideMenuManager.default.menuPushStyle = .preserveAndHideBackButton
        SideMenuManager.default.menuAnimationFadeStrength = 0.1
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false
        SideMenuManager.default.menuDismissOnPush = true
        SideMenuManager.default.menuAlwaysAnimate = true
    }

}
