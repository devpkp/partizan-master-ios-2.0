//
//  InitViewExtension.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import UserNotifications
import SideMenu
import FirebaseMessaging
import Firebase



// MARK: - completeView

extension MapViewController {
    func addActivityIndicator() {
        view.addSubview(self.activityIndicator)
        activityIndicator.frame = self.view.bounds
        activityIndicator.startAnimating()
    }
    func removeActivityIndicator() {
        activityIndicator.removeFromSuperview()
    }
    
    func initCompleteView() {
        view.addSubview(completeView)
        
        labelCompleteView.translatesAutoresizingMaskIntoConstraints = false
        completeView.addSubview(labelCompleteView)
        labelCompleteView.leadingAnchor.constraint(equalTo: completeView.leadingAnchor).isActive = true
        labelCompleteView.trailingAnchor.constraint(equalTo: completeView.trailingAnchor).isActive = true
        labelCompleteView.topAnchor.constraint(equalTo: completeView.topAnchor).isActive = true
        labelCompleteView.bottomAnchor.constraint(equalTo: completeView.bottomAnchor).isActive = true
    }
    func completeSendViewAnimation() {
        let alert = UIAlertController(title: "Отчет успешно отправлен", message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            alert.dismiss(animated: true, completion: nil)
        }
        
    }
}


extension MapViewController {
    func fillLabelsOfOrder() {
        nameOfCustomerLabel.text = activeOrder?.clientName
        commentTextView.text = activeOrder?.description
        addressLabel.text = activeOrder?.address?.fullAddressDescriptor
        problemSdLabel.textColor = activeOrder?.suspicionForHD ?? false ? #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1) : #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        problemWithPOLabel.textColor = activeOrder?.problemWithSW ?? false ? #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1) : #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.title = "Заявка №\(activeOrder?.id ?? "-")"
        let canCall = ((activeOrder?.canCall ?? false) &&
            (activeOrder?.status != .sent) &&
            ((activeOrder?.startWorkTime ?? Date()) - Date() < 900))
        self.navigationItem.rightBarButtonItem = canCall ? phoneButton : nil
        if !popUpViewIsOpen{
            requestState.setTitle("Активная заявка", for: .normal)
        }
    }

    func addTargets() {
        acceptButton.addTarget(self, action: #selector(acceptJob), for: .touchUpInside)
        startJobButton.addTarget(self, action: #selector(startJob), for: .touchUpInside)
        refuseRequestButton.addTarget(self, action: #selector(refuseRequest), for: .touchUpInside)
        requestCompleteButton.addTarget(self, action: #selector(requestComplete), for: .touchUpInside)
    }

    func hidePopUpView() {
        UIView.animate(withDuration: 0.4) {
            if self.popUpViewIsOpen {
                self.popUpView.frame.origin.y += self.sizeOfPopup
            }
        }
        popUpViewIsOpen = false
    }

    func initView() {
        historyButton.isHidden = true
        timerLabel.isHidden = true
        
        popUpView.frame.size.width = view.frame.width - 16
        popUpView.frame.origin.y = view.frame.size.height - popUpButton.frame.size.height - requestState.frame.size.height - 8
        popUpView.frame.origin.x = 8
        view.addSubview(popUpView)
        
        historyView.frame.size.width = view.frame.size.width * 0.9
        historyView.frame.size.height = view.frame.size.height * 0.75
        historyView.center = view.center
        
        blurView.frame = view.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.alpha = 0.8

        historyTableView.delegate = self
        historyTableView.dataSource = self
        historyTableView.allowsSelection = false
        historyTableView.rowHeight = UITableView.automaticDimension
        historyTableView.estimatedRowHeight = 150.0
        historyTableView.tableFooterView = UIView()
        
        
    }
}
