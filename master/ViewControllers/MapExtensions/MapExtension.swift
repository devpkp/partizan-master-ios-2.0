//
//  MapExtension.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//



import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import UserNotifications
import SideMenu
import FirebaseMessaging
import Firebase

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        myLocation = location
        globalLocation = location
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        //FirebaseMethods.changeFieldsOfUser(["lastLocation": GeoPoint.init(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)])
        locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            print("\n\n\nauthorizedAlways")
        case .authorizedWhenInUse:
            print("\n\n\nauthorizedWhenInUse")
        case .denied:
            print("\n\n\ndenied")
        case .notDetermined:
            print("\n\n\nnotDetermined")
        case .restricted:
            print("\n\n\nrestricted")
        }
    }
}

// MARK: - Geocode
extension MapViewController {
    
    func moveGeoPoint(isOpen: Bool) {
        guard let locationCoordinate = self.myLocation?.coordinate else { return }
        var point = self.mapView.projection.point(for: locationCoordinate)
        point.y = point.y + (isOpen ? 50 : 0)
        mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 20,
                                       bottom: 60 + (isOpen ? sizeOfPopup : 0) , right: 20)
        let newPoint = self.mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint)
        self.mapView.animate(with: camera)
    }
    
    func mapInit() {
        mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 20,
                                       bottom: 60 , right: 20)
        locationManager.delegate = self
        //locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        mapView.delegate = self
    }
    
    func removeMarker() {
        mapView.clear()
    }
    
    func showPath(polyStr: String) {
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = #colorLiteral(red: 0.5137254902, green: 0.7725490196, blue: 0.003921568627, alpha: 1)
        polyline.map = mapView
    }
    
    
    func geocoding(title: String, address: Address, route: Bool) {
        Geocoding.geocoding(address: address) { [weak self] (location, error) in
            guard let location = location else {
                print("[geocoding] no location")
                //                ShowAlerts.showAlert(viewControllerToShow: noPathViewController, headerTitle: Constants.errorAlertMessage, message: Constants.canNotShowPath, okTitle: Constants.okString)
                return
            }
            self?.removeMarker()
            let marker = GMSMarker(position: location)
            marker.title = title
            marker.map = self?.mapView
            if route {
                guard let myCurrentLocation = self?.myLocation else { return }
                Geocoding.getPolylineRoute(from: location, to: myCurrentLocation.coordinate) { [weak self] points, update in
                    self?.showPath(polyStr: points)
                    self?.mapView?.moveCamera(update)
                }
            }
        }
    }
}
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let alert = PazNavigationApp.directionsAlertController(coordinate: marker.position, name: "", title: Constants.chooseApp, message: "", completion: nil)
        
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        
        self.present(alert, animated: true, completion: nil)
        
        
        // present(alert, animated: true)
        return true
    }
}
