//
//  HistoryMapExtension.swift
//  master
//
//  Created by Sergey Gusev on 31/10/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation
import UIKit
extension MapViewController {
    @IBAction func historyButtonTapped(_ sender: UIButton) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.view.addSubview(self.blurView)
            self.view.addSubview(self.historyView)
            self.historyTableView.reloadData()
        }, completion: nil)
    }
    
    @IBAction func closeHistoryButtonTapped(_ sender: UIButton) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.historyView.removeFromSuperview()
            self.blurView.removeFromSuperview()
        }, completion: nil)
    }
}
extension MapViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let historyReports = activeOrder?.relatedReportsIds else { return 0 }
        tableView.backgroundView = historyReports.isEmpty ? label : nil
        return historyReports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as? HistoryOrderTableViewCell else {return UITableViewCell()}
        cell.tag = indexPath.row
        guard let historyReports = activeOrder?.relatedReportsIds else { return cell}
        if historyDictionary[indexPath.row] == nil {
            FirebaseMethods.getHistoryReports(historyReports: historyReports[indexPath.row]) { (history) in
                if cell.tag == indexPath.row {
                    self.historyDictionary[indexPath.row] = history
                    self.fillCell(cell: cell, history: history)
                }
            }
        } else {
            fillCell(cell: cell, history: historyDictionary[indexPath.row])
        }
        return cell
    }
    func fillCell(cell: HistoryOrderTableViewCell, history: HistoryReports?) {
        cell.nameOfClient.text = history?.nameOfClient
        cell.date.text = self.dateFormatter.string(from: history?.date ?? Date())
        cell.status.text = history?.status
        cell.nameOfMaster.text = history?.nameOfMaster
        cell.summa.text = String(describing: history?.summa ?? 0)
        cell.commentOfClient.text = history?.commentOfClient
        cell.commentOfMaster.text = history?.commentOfMaster
    }
}
