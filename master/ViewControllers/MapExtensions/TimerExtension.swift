//
//  TimerExtension.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import UserNotifications
import SideMenu
import FirebaseMessaging
import Firebase

extension MapViewController {
    func startTimer(timeForWork : Date, isBack: Bool) {
        if !isTimerRunning {
            timeForTimer = isBack ? Int(timeForWork.timeIntervalSince1970 - Date().timeIntervalSince1970) : Int(Date().timeIntervalSince1970 - timeForWork.timeIntervalSince1970)
            isTimerRunning = true
            timerLabel.isHidden = false
            timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(updateTimer(_:)), userInfo: isBack, repeats: true)
        }
    }

    func timeString(time: Int) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }

    @objc func stopTimer() {
        timer.invalidate()
        isTimerRunning = false

    }

    @objc func updateTimer(_ timer: Timer) {

        if timeForTimer < 1 {
            stopTimer()
            timerLabel.text = "Время закончилось"
        } else {
            let isBack = timer.userInfo as? Bool ?? true
            if isBack {
                timeForTimer -= 1
            } else {
                timeForTimer += 1
            }
            timerLabel.text = timeString(time: timeForTimer)
        }
    }
}
