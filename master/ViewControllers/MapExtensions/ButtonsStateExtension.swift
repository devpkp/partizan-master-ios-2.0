//
//  ButtonsStateExtension.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import UserNotifications
import SideMenu
import FirebaseMessaging
import Firebase

// MARK: - Change state of buttons
extension MapViewController {

    enum StateButtons{
        case accept
        case start
        case refuse
        case complete
    }
    func addAlert(state: StateButtons, message: String, textRefuse: String? = nil, status: StatusOfReport? = nil){
        let alert = UIAlertController(title: Constants.attensionAlertMessage, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.no, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.yes, style: .default, handler: { [weak self] (_) in
            
            guard let id = self?.activeOrder?.id,
                let number = self?.activeOrder?.number,
                let description = self?.activeOrder?.description,
                let address = self?.activeOrder?.address,
                let userName = user?.firstName,
                let clientName = self?.activeOrder?.clientName else {
                    print("Failure")
                    return
            }
            self?.addActivityIndicator()
            FirebaseMethods.checkValidOrder(id) { [weak self] isValid in
                self?.removeActivityIndicator()
                guard let alertViewController = self else {
                    print("[MapViewController.addAlert] no viewControllers")
                    return
                }
                if !isValid {
                    
                    ShowAlerts.showAlert(viewControllerToShow: alertViewController, headerTitle: Constants.attensionAlertMessage, message: Constants.checkCurrentOrder, okTitle: Constants.okString)
                    return
                }
                guard let onlineStatus = user?.online,
                    onlineStatus == true else {
                        ShowAlerts.showAlert(viewControllerToShow: alertViewController, headerTitle: Constants.attensionAlertMessage, message: Constants.checkOnlineStatus, okTitle: Constants.okString)
                        return
                }
                switch state {
                case .accept:
                    FirebaseMethods.updateOrder(dictionary: ["status":"accepted"], documentId: id)
                    FirebaseMethods.updateLocationUser(event: LocationEventStatus.orderAccepted.rawValue, location: self?.myLocation, orderId: id, userId: FirebaseMethods.getUID())
                case .start:
                    let currentDate = Date(timeIntervalSinceNow: -10)
                    FirebaseMethods.updateOrder(dictionary: ["status":"processing",
                                                             "timerWasLaunched": currentDate],
                                                documentId: id)
                    
                    FirebaseMethods.updateLocationUser(event: LocationEventStatus.orderStarted.rawValue, location: self?.myLocation, orderId: id, userId: FirebaseMethods.getUID())
                case .refuse:
                    let idReport = FirebaseMethods.generateReportId()
                    let refuseReport = Report(bsoNumber: nil, comments: nil, cost: nil, agreementDate: nil, defects: nil, agreementNumber: nil, orderId: id, reportIsAccepted: false, status: status, timeSpend: nil, productType: nil, usedDetails: nil, userId: FirebaseMethods.getUID(), cancelOrDeclineReason: textRefuse, clientName: clientName, userName: userName, orderNumber: number, orderDescription: description, orderAddress: address, date: Date())
                    FirebaseMethods.writeToReports(id: idReport, report: refuseReport, finish: { (downloadedIdReport) in
                        print(downloadedIdReport)
                    })
                    FirebaseMethods.updateOrder(dictionary: ["status":"awaiting"], documentId: id)
                    FirebaseMethods.updateLocationUser(event: LocationEventStatus.reportCreated.rawValue, location: self?.myLocation, orderId: id, userId: FirebaseMethods.getUID())
                case .complete:
                    break
                }
            }
            
        }))
        present(alert, animated: true)
    }
    func startStateOfButtonsInPopUpView(expireTime: Date) {
        stopTimer()
        stateOfPopUpView(timer: false, accept: false, start: true, refuse: true, requestComplete: true)
        startTimer(timeForWork: expireTime, isBack: true)
    }

    @objc func acceptJob() {
        addAlert(state: .accept, message: Constants.areYouSureAcceptOrder)
    }
    func stateOfPopUpView(timer: Bool, accept: Bool, start: Bool, refuse: Bool, requestComplete: Bool) {
        timerLabel.isHidden = timer
        acceptButton.isHidden = accept
        startJobButton.isHidden = start
        refuseRequestButton.isHidden = refuse
        requestCompleteButton.isHidden = requestComplete
    }
    @objc func startJob() {
        addAlert(state: .start, message: Constants.areYouSureStartWork)
    }

    @objc func refuseRequest() {
        let refuses = ["Не открывают дверь", "Не устроила цена", "Передумал чинить"]
        let alert = UIAlertController(title: Constants.chooseReason, message: nil, preferredStyle: .actionSheet)
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alert.addAction(UIAlertAction(title: refuses[0], style: .default, handler: { [weak self]  (_) in
            self?.refuse(textRefuse: refuses[0], status: .declined)
        }))
        alert.addAction(UIAlertAction(title: refuses[1], style: .default, handler: { [weak self]  (_) in
            self?.refuse(textRefuse: refuses[1], status: .declined)
        }))
        alert.addAction(UIAlertAction(title: refuses[2], style: .default, handler: { [weak self]  (_) in
            self?.refuse(textRefuse: refuses[2], status: .declined)
        }))
        alert.addAction(UIAlertAction(title: Constants.cancel, style: .destructive, handler: nil))
        present(alert, animated: true)
    }

    func refuse(textRefuse: String, status: StatusOfReport) {
         addAlert(state: .refuse, message: Constants.areYouSureToRefuseOrder, textRefuse: textRefuse, status: status)
    }
    
    @objc func requestComplete() {
        guard let onlineStatus = user?.online,
            onlineStatus == true else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.attensionAlertMessage, message: Constants.checkOnlineStatus, okTitle: Constants.okString)
                return
        }
        
        guard let id = self.activeOrder?.id,
            let number = self.activeOrder?.number,
            let description = self.activeOrder?.description,
            let address = self.activeOrder?.address,
            let userName = user?.firstName,
            let clientName = self.activeOrder?.clientName else {
                print("Failure")
                return
        }
       // self.stopTimer()
        //FirebaseMethods.updateLocationUser(event: "requestComplete", location: self.myLocation, orderId: id, userId: FirebaseMethods.getUID())
        guard let sendReportVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sendReportViewCotroller") as? SendReportViewController else {return}
        let navigationController = self.navigationController
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        navigationController?.view.layer.add(transition, forKey: nil)
        
        sendReportVC.commonInit(orderId: id, orderNumber: number, orderDescription: description, orderAddress: address, timeOfWork: self.timeForTimer, clientName: clientName, userName: userName)
        
        navigationController?.pushViewController(sendReportVC, animated: false)
    }
}
