//
//  DataStateExtension.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//
import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import UserNotifications
import SideMenu
import FirebaseMessaging
import Firebase

// MARK: - Get data
extension MapViewController {
    
    func emptyOrders() {
        popUpView.isUserInteractionEnabled = false
        navigationItem.rightBarButtonItem =  nil
        if popUpViewIsOpen {
            upButtonTapped(UIButton())
        }
        requestState.setTitle("Ожидайте", for: .normal)
        self.title = ""
    }

    func checkStatusOfOrder() {
        removeMarker()
        if orders.isEmpty {
            print("[checkStatusOfOrder] is Empty")
            emptyOrders()
            return
        } else {
            
            popUpView.isUserInteractionEnabled = true
            activeOrder = self.orders.first
            fillLabelsOfOrder()
        }
        guard let number = activeOrder?.number,
            let expireTime = activeOrder?.expireTime,
            let startWorkTime = activeOrder?.startWorkTime,
            let statusOfOrder = activeOrder?.status,
            let address = activeOrder?.address,
            let notificationState = activeOrder?.notificationState,
            let id = activeOrder?.id else {
                print("[checkStatusOfOrder] error of reading parameters")
                return
        }
        if notificationState == .unnotified {
            ShowAlerts.showAlertAction(viewControllerToShow: self, headerTitle: Constants.attensionAlertMessage, message: Constants.accept, okTitle: Constants.okString) {
                FirebaseMethods.updateOrder(dictionary: ["notificationState": NotificationState.notified.rawValue], documentId: id)
            }
        }
        switch statusOfOrder {
        case .sent:
            print("[checkStatusOfOrder] status is \(String(describing: activeOrder?.status))")
            startStateOfButtonsInPopUpView(expireTime: expireTime)
            geocoding(title: number, address: address, route: false)
            if activeOrder?.orderWasSeen == nil {
                FirebaseMethods.updateOrder(dictionary: ["orderWasSeen": Date()], documentId: id)
            }
        case .accepted:
            print("[checkStatusOfOrder] status is \(String(describing: activeOrder?.status))")
            stopTimer()
            stateOfPopUpView(timer: false, accept: true, start: false, refuse: false, requestComplete: true)
            startTimer(timeForWork: startWorkTime, isBack: true)
            geocoding(title: number, address: address, route: true)
        case .processing:
            print("[checkStatusOfOrder] status is \(String(describing: activeOrder?.status))")
            stopTimer()
            stateOfPopUpView(timer: false, accept: true, start: true, refuse: true, requestComplete: false)
            guard let timerWasLaunched = activeOrder?.timerWasLaunched else {
                print("[checkStatusOfOrder] error of reading parameters")
                return }
            startTimer(timeForWork: timerWasLaunched, isBack: false)
        default:
            break
        }
    }
}
