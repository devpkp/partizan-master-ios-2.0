//
//  SdReportViewController.swift
//  master
//
//  Created by Polina Guryeva on 10.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class SdReportViewController: SuperSendReportViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataToSendSdReport: [Int: String] = [:]
    var dateComponent: DateComponents!
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    let progressView = UIProgressView(progressViewStyle: .bar)
    let alert = UIAlertController(title: Constants.pleaseWait, message: Constants.loadingFiles, preferredStyle: .alert)
    
    var agreementNumberTextField: UITextField!
    var dateTextField: UITextField!
    var typeOfDetailTextField: UITextField!
    var defectTextField: UITextField!
    var commentTextField: UITextView!
    
    var sendReportButton: UIButton!
    
    
    var orderId: String?
    var orderNumber: String?
    var orderDescription: String?
    var orderAddress: Address?
    var timeOfWork: Int?
    var clientName: String?
    var userName: String?
    var idReport: String?
    
    var allPhotos: [String] = []
    
    var photoDocumentsCell: AddPhotoTableViewCell?
    var photoBillCell: AddPhotoTableViewCell?
    var photoDevCell: AddPhotoTableViewCell?
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter
    }()
    
    lazy var toolBar: UIToolbar = {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonTapped))
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }()
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50.0
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        
        idReport = FirebaseMethods.generateReportId()
        
        dataToSendSdReport[2] = dateFormatter.string(from: Date())
    }
    
    func commonInit(orderId: String, orderNumber: String, orderDescription: String, orderAddress: Address, timeOfWork: Int, clientName: String, userName: String) {
        self.orderId = orderId
        self.orderNumber = orderNumber
        self.orderDescription = orderDescription
        self.orderAddress = orderAddress
        self.timeOfWork = timeOfWork
        
        self.clientName = clientName
        self.userName = userName
    }
    func parsePhotoNames() -> ([String], [String], [String]) {
        guard var photoDocumentsItems = photoDocumentsCell?.names,
            var photoDevItems = self.photoDevCell?.names,
            var photoBillItems = self.photoBillCell?.names else {
                return ([""], [""], [""])
        }
        
        photoDocumentsItems.remove(at: 0)
        photoDevItems.remove(at: 0)
        photoBillItems.remove(at: 0)
        return (photoDocumentsItems, photoDevItems, photoBillItems)
    }
    func checkFields() {
        let check = [1, 2, 4, 5, 12]
        var pleaseFillField: [Int] = []
        let arrayOfKeys = Array(dataToSendSdReport.keys)
        for item in check {
            if arrayOfKeys.contains(item) {
                if dataToSendSdReport[item]!.isEmpty {
                    pleaseFillField.append(item)
                }
            }
            else {
                pleaseFillField.append(item)
            }
        }
        if let count = photoDocumentsCell?.items.count, count < 2 {
            pleaseFillField.append(-1) // for photo
        }
        if let count = photoDevCell?.items.count, count < 2 {
            pleaseFillField.append(-2)
        }
        
        if !(pleaseFillField.isEmpty) {
            var emptyFields = "Необходимо заполнить следующие поля:\n"
            for item in pleaseFillField {
                switch item {
                case 1:
                    emptyFields += "\"Номер соглашения\""
                case 2:
                    emptyFields += "\"Дата\""
                case 4:
                    emptyFields += "\"Тип изделия\""
                case 5:
                    emptyFields += "\"Дефекты\""
                case 12:
                    emptyFields += "\"Комментарий\""
                case -1:
                    emptyFields += "\"Фото документов\""
                case -2:
                    emptyFields += "\"Фото техники\""
                default:
                    continue
                }
                emptyFields += "\n"
            }
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: emptyFields, okTitle: Constants.okString)
            return
        }
    }
    
    @objc func sendReport() {
        view.endEditing(true)
        checkFields()
        
        guard let agreementNumber = dataToSendSdReport[1], !agreementNumber.isEmpty else {return}
        guard let date = dataToSendSdReport[2], !date.isEmpty else {return}
        guard let typeOfDetail = dataToSendSdReport[4], !typeOfDetail.isEmpty else {return}
        guard let defect = dataToSendSdReport[5], !defect.isEmpty else {return}
        guard let comment = dataToSendSdReport[12], !comment.isEmpty else {return}
        
        guard comment.count < 254,
            typeOfDetail.count < 254,
            defect.count < 254 else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCount, okTitle: Constants.okString)
            return
        }
        
         if !(agreementNumber.isConform(to: "[^0-9 ]") &&
            date.isConform(to: "[^0-9./ ]") &&
            typeOfDetail.isConform(to: "[^a-zA-Zа-яА-ЯёЁ ,-:+]") &&
            defect.isConform(to: "[^a-zA-Zа-яА-ЯёЁ ,.-:+?!]") &&
            comment.isConform(to: "[^a-zA-Zа-яА-ЯёЁ()0-9 .,-?!;:+%]")) {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
        }
        guard agreementNumber.count < 11,
            date.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongInt, okTitle: Constants.okString)
                return
        }

        let (photoDocumentsItems, photoDevItems, photoBillItems) = parsePhotoNames()
        guard photoDocumentsItems.count < 11,
            photoDevItems.count < 11,
            photoBillItems.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongPhotos, okTitle: Constants.okString)
                return
        }
        allPhotos = photoDocumentsItems + photoDevItems + photoBillItems
        
        let idReportToSend = self.idReport ?? "NULL_ID_REPORT_\(Date())"
        
//        let imagesWithNames = initImageArray(documents: photoDocumentsItems, bills: photoDevItems, devs: photoBillItems, idReport: idReportToSend)
        
        let reportSd = Report(bsoNumber: nil, comments: comment, cost: nil, agreementDate: Date(), defects: defect, agreementNumber: agreementNumber, orderId: orderId, reportIsAccepted: false, status: .sd, timeSpend: nil, productType: typeOfDetail, usedDetails: nil, userId: FirebaseMethods.getUID(), cancelOrDeclineReason: nil, clientName: clientName, userName: userName,  orderNumber: orderNumber, orderDescription: orderDescription, orderAddress: orderAddress, date: Date())
        
        areYouSureToSendReportAlert(vc: self, idChecker: orderId) {
            
            self.addActivityIndicator()
            
            self.addProgressView()
            PostService.create(for: self.allPhotos, progressView: self.progressView) { (check) in
                if check {
                    FirebaseMethods.writeToReports(id: idReportToSend, report: reportSd, finish: { (lineString) in
                        print(lineString)
                        guard let id = self.orderId else {
                            print("[sendReport] error in getting id or order")
                            return }
                        FirebaseMethods.updateOrder(dictionary: ["status":"awaiting"], documentId: id)
                        self.activityIndicator.removeFromSuperview()
                       // self.progressView.removeFromSuperview()
                        self.alert.dismiss(animated: true, completion: nil)
                        self.popToMapViewController()
                    })
                } else {
                    self.failureUploading()
                }
            }
        }
    }
    
    
    
    func changeProgressView(to progress: Float) {
        progressView.setProgress(progress / Float(3), animated: true)
    }
    
    func addActivityIndicator() {
        view.addSubview(self.activityIndicator)
        activityIndicator.frame = self.view.bounds
        activityIndicator.startAnimating()
    }
    func addProgressView() {
        progressView.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
        progressView.trackTintColor = .lightGray
        progressView.tintColor = .blue
        alert.view.addSubview(progressView)
        present(alert, animated: true)
        
//        progressView.center = self.view.center
//        progressView.trackTintColor = .lightGray
//        progressView.tintColor = .blue
//        view.addSubview(progressView)
    }
    func failureUploading() {
        activityIndicator.removeFromSuperview()
        progressView.removeFromSuperview()
        ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.canNotLoadPhoto, message: Constants.tryAgain, okTitle: Constants.okString)
    }
    deinit {
        print("[SdReportViewController] deinit")
        if !allPhotos.isEmpty{
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        } else {
            let (photoDocumentsItems, photoDevItems, photoBillItems) = parsePhotoNames()
            allPhotos = photoDocumentsItems + photoDevItems + photoBillItems
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        }
    }
    
}

extension SdReportViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    @objc func doneButtonTapped() {
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        dateComponent = datePicker.calendar.dateComponents([.day, .month, .year], from: datePicker.date)
        typeOfDetailTextField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = UITableViewCell()
            cell.commonInit(text: "Соглашение * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Номер соглашения"
            
            
            agreementNumberTextField = cell.textField
            
            agreementNumberTextField.delegate = self
            agreementNumberTextField.tag = indexPath.section
            //agreementNumberTextField.inputView = nil
            agreementNumberTextField.text = dataToSendSdReport[indexPath.section]
            agreementNumberTextField.keyboardType = .numberPad
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Дата"
            
            dateTextField = cell.textField
            dateTextField.delegate = self
            dateTextField.tag = indexPath.section
            dateTextField.text = dataToSendSdReport[indexPath.section]
            dateTextField.inputView = datePicker
            dateTextField.inputAccessoryView = toolBar
            return cell
            
        case 3:
            let cell = UITableViewCell()
            cell.commonInit(text: "Описание * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Тип изделия"
            typeOfDetailTextField = cell.textField
            typeOfDetailTextField.delegate = self
            typeOfDetailTextField.tag = indexPath.section
            typeOfDetailTextField.text = dataToSendSdReport[indexPath.section]
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Дефекты"
            defectTextField = cell.textField
            defectTextField.delegate = self
            defectTextField.tag = indexPath.section
            defectTextField.text = dataToSendSdReport[indexPath.section]
            return cell
            
        case 6:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото документов * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell

        case 8:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото техники * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell

        case 10:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото чека:", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: false)
            return cell

        case 7:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_doc")
            photoDocumentsCell = cell
            return cell

        case 9:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_dev")
            photoDevCell = cell
            return cell

        case 11:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_bill")
            
            photoBillCell = cell
            return cell
            
        case 12:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "commentTableViewCell") as? CommentTableViewCell else {return UITableViewCell()}
            cell.comment.text = ""
            commentTextField = cell.comment
            commentTextField.delegate = self
            commentTextField.tag = indexPath.section
            commentTextField.text = dataToSendSdReport[indexPath.section]
            return cell                                                                                             
            
        case 13:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportButtonTableViewCell") as? ReportButtonTableViewCell else {return UITableViewCell()}
            cell.button.setTitle("Отправить", for: .normal)
            sendReportButton = cell.button
            sendReportButton.addTarget(self, action: #selector(sendReport), for: .touchUpInside)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

// MARK: - text field delegate
extension SdReportViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        dataToSendSdReport[textField.tag] = text
        print(text + " of \(textField.tag) tag")
    }
}

// MARK: - text view delegate
extension SdReportViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let text = textView.text else {return}
        dataToSendSdReport[textView.tag] = text
    }
}
