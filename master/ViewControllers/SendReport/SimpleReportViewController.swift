//
//  SimpleReportViewController.swift
//  master
//
//  Created by Polina Guryeva on 10.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class SimpleReportViewController: SuperSendReportViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var dataToSendSimpleReport: [Int: String] = [:]
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    let progressView = UIProgressView(progressViewStyle: .bar)
    let alert = UIAlertController(title: Constants.pleaseWait, message: Constants.loadingFiles, preferredStyle: .alert)
    
    var detailItems = [DetailItem(detailName: "", detailCount: "", detailPrice: "")]
    weak var bsoNumberTextField: UITextField!
    weak var totalCostTextField: UITextField!
    weak var commentTextField: UITextView!
    
    weak var sendReportButton: UIButton!
    weak var addNewDetailButton: UIButton!
    
    var orderId: String?
    var orderNumber: String?
    var orderDescription: String?
    var orderAddress: Address?
    var timeOfWork: Int?
    
    var clientName: String?
    var userName: String?
    
    var idReport: String?
    
    var allPhotos: [String] = []
    
    weak var photoDocumentsCell: AddPhotoTableViewCell?
    weak var photoBillCell: AddPhotoTableViewCell?
    weak var photoDevCell: AddPhotoTableViewCell?
    
    var autoFillString = ""
    var reportWithoutDocuments = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        
        idReport = FirebaseMethods.generateReportId()
    }
    
    
    func commonInit(orderId: String, orderNumber: String, orderDescription: String, orderAddress: Address, timeOfWork: Int, clientName: String, userName: String) {
        self.orderId = orderId
        self.orderNumber = orderNumber
        self.orderDescription = orderDescription
        self.orderAddress = orderAddress
        self.timeOfWork = timeOfWork
        
        self.clientName = clientName
        self.userName = userName
    }
    
    func checkFields() {
        var check = [5, 6, 13]
        for i in 0..<detailItems.count {
            check.append((i + 1) * 100)
            check.append((i + 1) * 100 + 1)
            check.append((i + 1) * 100 + 2)
        }
        
        
        var pleaseFillField: [Int] = []
        let arrayOfKeys = Array(dataToSendSimpleReport.keys)
        for item in check {
            if arrayOfKeys.contains(item) {
                if dataToSendSimpleReport[item]!.isEmpty {
                    pleaseFillField.append(item)
                }
            }
            else {
                pleaseFillField.append(item)
            }
        }
        if let count = photoDocumentsCell?.items.count, count < 2, !reportWithoutDocuments {
            pleaseFillField.append(-1) // for photo
        }
        if let count = photoDevCell?.items.count, count < 2 {
            pleaseFillField.append(-2) // for photo
        }
        
        if !(pleaseFillField.isEmpty) {
            var emptyFields = "Необходимо заполнить следующие поля:\n"
            for item in pleaseFillField {
                switch item {
                case 5:
                    emptyFields += "\"Номер БСО\""
                case 6:
                    emptyFields += "\"Сумма\""
                case 13:
                    emptyFields += "\"Комментарий\""
                case -1:
                    emptyFields += "\"Фото документов\""
                case -2:
                    emptyFields += "\"Фото техники\""
                case let x where (x >= 100) && (x % 10 == 0):
                    emptyFields += "\"Название деталей\" в детали №\(x / 100)"
                case let x where (x >= 100) && (x % 10 == 1):
                    emptyFields += "\"Стоимость деталей\" в детали №\(x / 100)"
                case let x where (x >= 100) && (x % 10 == 2):
                    emptyFields += "\"Количество деталей\" в детали №\(x / 100)"
                default:
                    continue
                }
                emptyFields += "\n"
            }
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: emptyFields, okTitle: Constants.okString)
            return
        }
    }
    func parsePhotoNames() -> ([String], [String], [String]) {
        guard var photoDocumentsItems = photoDocumentsCell?.names,
            var photoDevItems = self.photoDevCell?.names,
            var photoBillItems = self.photoBillCell?.names else {
                return ([""], [""], [""])
        }
        
        photoDocumentsItems.remove(at: 0)
        photoDevItems.remove(at: 0)
        photoBillItems.remove(at: 0)
        return (photoDocumentsItems, photoDevItems, photoBillItems)
    }
    
    func sendReport() {
        
        checkFields()
        
        guard let bsoNumber = dataToSendSimpleReport[5], !bsoNumber.isEmpty else {return}
        guard let totalCost = dataToSendSimpleReport[6], !totalCost.isEmpty else {return}
        guard let comment = dataToSendSimpleReport[13], !comment.isEmpty else {return}
        
        guard comment.count < 254 else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCount, okTitle: Constants.okString)
            return
        }
        
        if !(bsoNumber.isConform(to: "[^0-9 ]") &&
            totalCost.isConform(to: "[^0-9., ]") &&
            comment.isConform(to: "[^a-zA-Zа-яА-ЯёЁ()0-9 .,-?!;:+%]")) {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
        }
        guard bsoNumber.count < 11 else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongInt, okTitle: Constants.okString)
            return
        }
        guard totalCost.count < 8 else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCost, okTitle: Constants.okString)
            return
        }
        var details: [UsedDetails] = []
        for i in 0..<detailItems.count {
            guard let detailName = dataToSendSimpleReport[(i + 1) * 100], !detailName.isEmpty else {return}
            guard let detailCount = dataToSendSimpleReport[(i + 1) * 100 + 1], !detailCount.isEmpty else {return}
            guard let detailPrice = dataToSendSimpleReport[(i + 1) * 100 + 2], !detailPrice.isEmpty else {return}
            
            if !(detailName.isConform(to: "[^a-zA-Zа-яА-ЯёЁ .,:-+]") &&
                detailCount.isConform(to: "[^0-9 ]") &&
                detailPrice.isConform(to: "[^0-9., ]")) {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
            }
            guard detailName.count < 254 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCount, okTitle: Constants.okString)
                return
            }
            guard detailCount.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongInt, okTitle: Constants.okString)
                return
            }
            guard detailPrice.count < 8 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCost, okTitle: Constants.okString)
                return
            }
            guard let count = Int(detailCount) else { return }
            guard let price = Double(detailPrice) else {return}
            details.append(UsedDetails(name: detailName, price: price, quantity: count))
        }
        
        let (photoDocumentsItems, photoDevItems, photoBillItems) = parsePhotoNames()
        guard photoDocumentsItems.count < 11,
            photoDevItems.count < 11,
            photoBillItems.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongPhotos, okTitle: Constants.okString)
                return
        }
        allPhotos = photoDocumentsItems + photoDevItems + photoBillItems
        var photosForSend = photoDevItems + photoBillItems
        if !reportWithoutDocuments {
            photosForSend += photoDocumentsItems
        }
        let idReportToSend = self.idReport ?? "NULL_ID_REPORT_\(Date())"
        
        
        //let imagesWithNames = initImageArray(documents: photoDocumentsItems, bills: photoDevItems, devs: photoBillItems, idReport: idReportToSend)
        
        let reportSimple = Report(bsoNumber: bsoNumber, comments: comment, cost: Double(totalCost), agreementDate: nil, defects: nil, agreementNumber: nil, orderId: orderId, reportIsAccepted: false, status: .completed, timeSpend: timeOfWork, productType: nil, usedDetails: details, userId: FirebaseMethods.getUID(), cancelOrDeclineReason: nil, clientName: clientName, userName: userName,  orderNumber: orderNumber, orderDescription: orderDescription, orderAddress: orderAddress, date: Date())
        
        areYouSureToSendReportAlert(vc: self, idChecker: orderId) {
            
            self.addActivityIndicator()
            
            self.addProgressView()
            PostService.create(for: photosForSend, progressView: self.progressView) { [weak self] (check) in
                if check {
                    FirebaseMethods.writeToReports(id: idReportToSend, report: reportSimple, finish: { [weak self] (lineString) in
                        print(lineString)
                        guard let id = self?.orderId else {
                            print("[sendReport] error in getting id or order")
                            return }
                        FirebaseMethods.updateOrder(dictionary: ["status":"awaiting"], documentId: id)
                        self?.activityIndicator.removeFromSuperview()
                        // self?.progressView.removeFromSuperview()
                        self?.alert.dismiss(animated: true, completion: nil)
                        self?.popToMapViewController()
                    })
                } else {
                    self?.failureUploading()
                }
            }
        }
    }
    
    
    func changeProgressView(to progress: Float) {
        progressView.setProgress(progress / Float(3), animated: true)
    }
    func addActivityIndicator() {
        view.addSubview(self.activityIndicator)
        activityIndicator.frame = self.view.bounds
        activityIndicator.startAnimating()
    }
    func addProgressView() {
        progressView.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
        progressView.trackTintColor = .lightGray
        progressView.tintColor = .blue
        alert.view.addSubview(progressView)
        present(alert, animated: true)
        
    }
    func failureUploading() {
        activityIndicator.removeFromSuperview()
        progressView.removeFromSuperview()
        ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.canNotLoadPhoto, message: Constants.tryAgain, okTitle: Constants.okString)
    }
    
    @IBAction func switchDidSwitched(_ sender: UISwitch) {
        reportWithoutDocuments = sender.isOn
        if sender.isOn {
            bsoNumberTextField.text = "0"
            dataToSendSimpleReport[5] = "0"
        } else {
            bsoNumberTextField.text = ""
            dataToSendSimpleReport[5] = ""
        }
        tableView.reloadData()
    }
    
    deinit {
        print("[SimpleReportViewController] deinit")
        
        if !allPhotos.isEmpty{
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        } else {
            let (photoDocumentsItems, photoDevItems, photoBillItems) = parsePhotoNames()
            allPhotos = photoDocumentsItems + photoDevItems + photoBillItems
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        }
        
    }
}

extension SimpleReportViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return detailItems.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = UITableViewCell()
            cell.commonInit(text: "Детали:", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: false)
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailTableViewCell") as? DetailTableViewCell else {return UITableViewCell()}
            cell.delegate = self
            cell.countDetailTextField.keyboardType = .numberPad
            cell.detailPriceTextField.keyboardType = .numberPad
            
            detailItems[indexPath.row].nameTextField  = cell.detailNameTextField
            detailItems[indexPath.row].countTextField = cell.countDetailTextField
            detailItems[indexPath.row].priceTextField = cell.detailPriceTextField
            
            detailItems[indexPath.row].nameTextField?.delegate  = self
            detailItems[indexPath.row].countTextField?.delegate = self
            detailItems[indexPath.row].priceTextField?.delegate = self
            
            detailItems[indexPath.row].nameTextField?.text  = dataToSendSimpleReport[(indexPath.row + 1) * 100]
            detailItems[indexPath.row].countTextField?.text = dataToSendSimpleReport[(indexPath.row + 1) * 100 + 1]
            detailItems[indexPath.row].priceTextField?.text = dataToSendSimpleReport[(indexPath.row + 1) * 100 + 2]
            
            detailItems[indexPath.row].nameTextField?.tag  = (indexPath.row + 1) * 100
            detailItems[indexPath.row].countTextField?.tag = (indexPath.row + 1) * 100 + 1
            detailItems[indexPath.row].priceTextField?.tag = (indexPath.row + 1) * 100 + 2
            
            cell.tableView = tableView
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportButtonTableViewCell") as? ReportButtonTableViewCell else {return UITableViewCell()}
            cell.delegate = self
            cell.tableView = tableView
            cell.button.setTitle("Добавить еще", for: .normal)
            return cell
            
        case 3:
            let cell = UITableViewCell()
            cell.commonInit(text: "Информация * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "checkBoxCell") as? CheckBoxTableViewCell else {return UITableViewCell()}
            cell.pointLabel.text = "Выполнил без документов"
            cell.skillSwitch.isOn = reportWithoutDocuments
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Номер БСО"
            cell.textField.keyboardType = .numberPad
            
            bsoNumberTextField = cell.textField
            bsoNumberTextField.delegate = self
            bsoNumberTextField.tag = indexPath.section
            bsoNumberTextField.text = dataToSendSimpleReport[indexPath.section]
            return cell
            
        case 6:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Сумма"
            cell.textField.keyboardType = .numberPad
            
            totalCostTextField = cell.textField
            totalCostTextField.delegate = self
            totalCostTextField.tag = indexPath.section
            totalCostTextField.text = dataToSendSimpleReport[indexPath.section]
            return cell
            
        case 7:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото документов * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 9:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото техники * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 11:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото чека:", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: false)
            return cell
            
        case 8:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_doc")
            photoDocumentsCell = cell
            return cell
            
        case 10:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_dev")
            
            photoDevCell = cell
            return cell
            
        case 12:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_bill")
            photoBillCell = cell
            return cell
            
        case 13:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "commentTableViewCell") as? CommentTableViewCell else {return UITableViewCell()}
            cell.comment.text = ""
            cell.delegate = self
            cell.tableView = tableView
            commentTextField = cell.comment
            commentTextField.delegate = self
            commentTextField.tag = indexPath.section
            commentTextField.text = dataToSendSimpleReport[indexPath.section]
            return cell
            
        case 14:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportButtonTableViewCell") as? ReportButtonTableViewCell else {return UITableViewCell()}
            cell.delegate = self
            cell.tableView = tableView
            cell.button.setTitle("Отправить", for: .normal)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    @objc func addDetailItem() {
        detailItems.append(DetailItem(detailName: "", detailCount: "", detailPrice: ""))
        tableView.insertRows(at: [IndexPath(row: detailItems.count - 1, section: 1)], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if reportWithoutDocuments &&
            (indexPath.section == 7 || indexPath.section == 8) {
            return 0.0
        }
        return tableView.rowHeight
    }
    
}

// MARK: - text field delegate
extension SimpleReportViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        dataToSendSimpleReport[textField.tag] = text
        print(text + " of \(textField.tag) tag")
    }
}

// MARK: - text view delegate
extension SimpleReportViewController: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let text = textView.text else {return}
        dataToSendSimpleReport[textView.tag] = text
    }
    
}

// MARK: - detail cell delegate
extension SimpleReportViewController: DetailCellDelegate {
    func didTapDeleteItem(_ tableView: UITableView, _ indexPath: IndexPath) {
        view.endEditing(true)
        
        for index in indexPath.row..<detailItems.count {
            dataToSendSimpleReport[(index + 1) * 100] = dataToSendSimpleReport[(index + 2) * 100]
            dataToSendSimpleReport[(index + 1) * 100 + 1] = dataToSendSimpleReport[(index + 2) * 100 + 1]
            dataToSendSimpleReport[(index + 1) * 100 + 2] = dataToSendSimpleReport[(index + 2) * 100 + 2]
        }
        detailItems.remove(at: indexPath.row)
        
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }
}

extension SimpleReportViewController: SendButtonDelegate {
    func didButtonTapped(_ tableView: UITableView, _ indexPath: IndexPath) {
        if indexPath.section == 2 {
            addDetailItem()
        }
        if indexPath.section == 14 {
            view.endEditing(true)
            self.sendReport()
        }
    }
    
    
}

extension SimpleReportViewController: AutoFillDelegate {
    
    func pushToAutoFillViewController() {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "refuseReportVC") as? RefuseReportViewController else {return}
        guard let navigator = navigationController else {return}
        vc.allowMultipleSelection = true
        vc.simpleReportVC = self
        navigator.pushViewController(vc, animated: true)
    }
    
    func didTappedAutoFill(_ tableView: UITableView, _ indexPath: IndexPath) {
        pushToAutoFillViewController()
    }
}
