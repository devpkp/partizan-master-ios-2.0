//
//  SendReportViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 23.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import Firebase

class SendReportViewController: SuperSendReportViewController {

    var menuItems = [MenuItem(icon: #imageLiteral(resourceName: "simpleReport"), label: "Сдать заявку"),
                     MenuItem(icon: #imageLiteral(resourceName: "sdReport"), label: "Сложная диагностика"),
                     MenuItem(icon: #imageLiteral(resourceName: "modern"), label: "Модернизация"),
                     MenuItem(icon: #imageLiteral(resourceName: "declined"), label: "Отказ"),
                     MenuItem(icon: #imageLiteral(resourceName: "declined"), label: "Отмена")]

    @IBOutlet weak var reportTypesTableView: UITableView!

    var orderId: String?
    var orderNumber: String?
    var orderDescription: String?
    var orderAddress: Address?
    var timeOfWork: Int?
    var clientName: String?
    var userName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        reportTypesTableView.delegate = self
        reportTypesTableView.dataSource = self
        reportTypesTableView.separatorStyle = .none
    }

    func commonInit(orderId: String, orderNumber: String, orderDescription: String, orderAddress: Address, timeOfWork: Int, clientName: String, userName: String) {
        self.orderId = orderId
        self.orderNumber = orderNumber
        self.orderDescription = orderDescription
        self.orderAddress = orderAddress
        self.timeOfWork = timeOfWork

        self.clientName = clientName
        self.userName = userName
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    deinit {
        print("[SendReportViewController] deinit")
    }
}

extension SendReportViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTypeCell") as? ReportTypeTableViewCell else {return UITableViewCell()}
        cell.settingIcon.image = menuItems[indexPath.row].icon
        cell.settingLabel.text = menuItems[indexPath.row].label

        switch indexPath.row {
        case 0:
            cell.backgroundColor = #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1)
        case 1:
            cell.backgroundColor = #colorLiteral(red: 0.1189137325, green: 0.4921377301, blue: 0.7745460868, alpha: 1)
        case 2:
            cell.backgroundColor = #colorLiteral(red: 0.1035201028, green: 0.4608638883, blue: 0.7349505424, alpha: 1)
        case 3:
            cell.backgroundColor = #colorLiteral(red: 0.1028571054, green: 0.4336505532, blue: 0.6952708364, alpha: 1)
        case 4:
            cell.backgroundColor = #colorLiteral(red: 0.09276203066, green: 0.402631551, blue: 0.6476410031, alpha: 1)
            view.backgroundColor = #colorLiteral(red: 0.09276203066, green: 0.402631551, blue: 0.6476410031, alpha: 1)
        default:
            break
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / CGFloat(menuItems.count)
    }
}

extension SendReportViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let navigator = navigationController else {return}
        tableView.deselectRow(at: indexPath, animated: true)
        guard let id = orderId,
            let number = orderNumber,
            let description = orderDescription,
            let adress = orderAddress,
            let time = timeOfWork,
            let clientNameForReport = clientName,
            let userNameForReport = userName else {return}
        switch indexPath.row {
        case 0:
            guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "simpleReportVC") as? SimpleReportViewController else {return}
            vc.commonInit(orderId: id, orderNumber: number, orderDescription: description, orderAddress: adress, timeOfWork: time, clientName: clientNameForReport, userName: userNameForReport)
            navigator.pushViewController(vc, animated: true)
        case 1:
            guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sdReportVC") as? SdReportViewController else {return}
            vc.commonInit(orderId: id, orderNumber: number, orderDescription: description, orderAddress: adress, timeOfWork: time, clientName: clientNameForReport, userName: userNameForReport)
            navigator.pushViewController(vc, animated: true)
        case 2:
            guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "modernVC") as? ModernizationViewController else {return}
            vc.commonInit(orderId: id, orderNumber: number, orderDescription: description, orderAddress: adress, timeOfWork: time, clientName: clientNameForReport, userName: userNameForReport)
            navigator.pushViewController(vc, animated: true)
        case 3:
            guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "refuseReportVC") as? RefuseReportViewController else {return}
            vc.commonInit(orderId: id, orderNumber: number, orderDescription: description, orderAddress: adress, timeOfWork: time, clientName: clientNameForReport, userName: userNameForReport, allowMultipleSelection: false)
            navigator.pushViewController(vc, animated: true)
        case 4:
            areYouSureToSendReportAlert(vc: self, idChecker: orderId) {
                let idReport = FirebaseMethods.generateReportId()
                let canceledReport = Report(bsoNumber: nil, comments: nil, cost: nil, agreementDate: nil, defects: nil, agreementNumber: nil, orderId: id, reportIsAccepted: false, status: .canceled, timeSpend: nil, productType: nil, usedDetails: nil, userId: FirebaseMethods.getUID(), cancelOrDeclineReason: nil, clientName: clientNameForReport, userName: userNameForReport, orderNumber: number, orderDescription: description, orderAddress: adress, date: Date())
                FirebaseMethods.writeToReports(id: idReport, report: canceledReport) { (downloadedIdReport) in
                    print(downloadedIdReport)
                }
                
                FirebaseMethods.updateOrder(dictionary: ["status":"awaiting"], documentId: id)
                self.popToMapViewController()
            }
        default:
            return
        }

    }
}
