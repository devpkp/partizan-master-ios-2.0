//
//  RefuseReportViewController.swift
//  master
//
//  Created by Polina Guryeva on 10.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class RefuseReportViewController: SuperSendReportViewController {

    weak var photoCell: AddPhotoTableViewCell?
    
    @IBOutlet weak var reasonsTableView: UITableView!
    var reasons = ["Отказ от СД", "Отказ от работ", "Отказ по цене", "Конфликт"]
    var patterns = ["винда", "дрова", "настройка ОС", "настройка ПО", "установка ПО", "настройка роутера", "настройка принтера", "чистка", "вирусы", "востановление данных", "подключение устройств"]
    var selectedIndex = 0
    var selectedIndecies = Set<Int>()
    
    var orderId: String?
    var orderNumber: String?
    var orderDescription: String?
    var orderAddress: Address?
    var timeOfWork: Int?

    var clientName: String?
    var userName: String?
    var idReport: String?
    
    var allowMultipleSelection = false

    var allPhotos: [String] = []
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    let alert = UIAlertController(title: Constants.pleaseWait, message: Constants.loadingFiles, preferredStyle: .alert)
    let progressView = UIProgressView(progressViewStyle: .bar)
    
    weak var simpleReportVC: SimpleReportViewController?
    weak var modernizationVC: ModernizationViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reasonsTableView.delegate = self
        reasonsTableView.dataSource = self
        reasonsTableView.tableFooterView = UIView()
        reasonsTableView.rowHeight = UITableView.automaticDimension
        reasonsTableView.estimatedRowHeight = 44.0

        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        idReport = FirebaseMethods.generateReportId()
    }

    func commonInit(orderId: String, orderNumber: String, orderDescription: String, orderAddress: Address, timeOfWork: Int, clientName: String, userName: String, allowMultipleSelection: Bool) {
        self.orderId = orderId
        self.orderNumber = orderNumber
        self.orderDescription = orderDescription
        self.orderAddress = orderAddress
        self.timeOfWork = timeOfWork

        self.clientName = clientName
        self.userName = userName
        self.allowMultipleSelection = allowMultipleSelection
    }
    func parsePhotoNames() -> [String] {
        guard var photoItems = photoCell?.names else {
                return [""]
        }
        
        photoItems.remove(at: 0)
        return photoItems
    }
    func checkFields() {
        var pleaseFillField: [Int] = []
        if let count = photoCell?.items.count, count < 2 {
            pleaseFillField.append(-1) // for photo
        }
        
        if !(pleaseFillField.isEmpty) {
            var emptyFields = "Необходимо заполнить следующие поля:\n"
            for item in pleaseFillField {
                switch item {
                case -1:
                    emptyFields += "\"Фото\""
                default:
                    continue
                }
                emptyFields += "\n"
            }
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: emptyFields, okTitle: Constants.okString)
            return
        }
    }

    @IBAction func sendReport(_ sender: UIButton) {
        checkFields()
        
        if allowMultipleSelection {
            var result = ""
            for item in selectedIndecies {
                result += " \(patterns[item])."
            }
            if let simple = simpleReportVC {
                simple.commentTextField.text += result
                simple.dataToSendSimpleReport[13] = simple.commentTextField.text
            } else if let modern = modernizationVC {
                modern.commentTextField.text += result
                modern.dataToSend[12] = modern.commentTextField.text
            }
            guard let navigator = navigationController else {return}
            navigator.popViewController(animated: true)
        } else {
            let photoItems = parsePhotoNames()
            guard photoItems.count < 11 else {
                    ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongPhotos, okTitle: Constants.okString)
                    return
            }
            allPhotos = photoItems
            let idReportToSend = self.idReport ?? "NULL_ID_REPORT_\(Date())"
            areYouSureToSendReportAlert(vc: self, idChecker: orderId) {
                
                self.addActivityIndicator()
                
                self.addProgressView()
                PostService.create(for: self.allPhotos, progressView: self.progressView) { (check) in
                    if check {
                        guard let cell = self.reasonsTableView.cellForRow(at: IndexPath(row: self.selectedIndex, section: 0)) as? ReasonLabelTableViewCell  else {return}
                        guard let reasonText = cell.reasonLabel.text else {return}
                        self.refuse(textRefuse: reasonText, status: .declined, id: idReportToSend)
                        self.popToMapViewController()
                        self.activityIndicator.removeFromSuperview()
                        // self.progressView.removeFromSuperview()
                        self.alert.dismiss(animated: true, completion: nil)
                    } else {
                        self.failureUploading()
                    }
                }
            }
        }
    }
    func refuse(textRefuse: String, status: StatusOfReport, id: String) {
        let refuseReport = Report(bsoNumber: nil, comments: nil, cost: nil, agreementDate: nil, defects: nil, agreementNumber: nil, orderId: orderId, reportIsAccepted: false, status: status, timeSpend: nil, productType: nil, usedDetails: nil, userId: FirebaseMethods.getUID(), cancelOrDeclineReason: textRefuse, clientName: clientName, userName: userName, orderNumber: orderNumber, orderDescription: orderDescription, orderAddress: orderAddress, date: Date())
        FirebaseMethods.writeToReports(id: id, report: refuseReport) { (downloadedIdReport) in
            print(downloadedIdReport)
        }
        guard let id = orderId else { return }
        FirebaseMethods.updateOrder(dictionary: ["status":"awaiting"], documentId: id)
    }
    func addActivityIndicator() {
        view.addSubview(self.activityIndicator)
        activityIndicator.frame = self.view.bounds
        activityIndicator.startAnimating()
    }
    func addProgressView() {
        progressView.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
        progressView.trackTintColor = .lightGray
        progressView.tintColor = .blue
        alert.view.addSubview(progressView)
        present(alert, animated: true)
        
        //        progressView.center = self.view.center
        //        progressView.trackTintColor = .lightGray
        //        progressView.tintColor = .blue
        //        view.addSubview(progressView)
    }
    func failureUploading() {
        activityIndicator.removeFromSuperview()
        progressView.removeFromSuperview()
        ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.canNotLoadPhoto, message: Constants.tryAgain, okTitle: Constants.okString)
    }
    deinit {
        print("[RefuseReportViewController] deinit")
        if !allPhotos.isEmpty{
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        } else {
            let photoItems = parsePhotoNames()
            allPhotos = photoItems
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        }
    }
}

extension RefuseReportViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allowMultipleSelection {
            return patterns.count
        } else {
            return reasons.count + 2
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // add photo label
        if !allowMultipleSelection && indexPath.row == reasons.count  {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reasonLabelCell") as? ReasonLabelTableViewCell else {return UITableViewCell()}
            let text = "Прикрепите фото * :"
            cell.reasonLabel.textColor = #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1)
            cell.reasonLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
            let mutableString = NSMutableAttributedString(string: text)
            mutableString.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5534581218, green: 0, blue: 0.1684132761, alpha: 1)], range: NSRange(location: text.count - 3, length: 1))
            cell.reasonLabel?.attributedText = mutableString
            cell.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
            return cell
        }
        
        // add photo cell
        if !allowMultipleSelection && indexPath.row == reasons.count + 1  {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_canc")
            cell.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
            photoCell = cell
            return cell
        }
        
        // reasons cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reasonLabelCell") as? ReasonLabelTableViewCell else {return UITableViewCell()}
        if allowMultipleSelection {
            cell.reasonLabel.text = patterns[indexPath.row]
            if selectedIndecies.contains(indexPath.row) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        } else {
            cell.reasonLabel.text = reasons[indexPath.row]
            if selectedIndex == indexPath.row{
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        return cell
    }
}

extension RefuseReportViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if allowMultipleSelection {
            if selectedIndecies.contains(indexPath.row) {
                selectedIndecies.remove(indexPath.row)
            } else {
                selectedIndecies.insert(indexPath.row)
            }
        } else  {
            if indexPath.row < reasons.count {
                selectedIndex = indexPath.row
            }
        }
        tableView.reloadData()
    }
}
