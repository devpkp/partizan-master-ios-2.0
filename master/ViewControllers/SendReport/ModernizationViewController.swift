//
//  ModernizationViewController.swift
//  master
//
//  Created by Polina Guryeva on 10.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class ModernizationViewController: SuperSendReportViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var dataToSend: [Int: String] = [:]
    
    var detailItems = [DetailItem(detailName: "", detailCount: "", detailPrice: "")]
    
    var bsoNumberTextField: UITextField!
    var commentTextField: UITextView!
    var prepaymentTextField: UITextField!
    
    var sendReportButton: UIButton!
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    let alert = UIAlertController(title: Constants.pleaseWait, message: Constants.loadingFiles, preferredStyle: .alert)
    
    let progressView = UIProgressView(progressViewStyle: .bar)
    
    var orderId: String?
    var orderNumber: String?
    var orderDescription: String?
    var orderAddress: Address?
    var timeOfWork: Int?
    var clientName: String?
    var userName: String?
    var idReport: String?
    
    var allPhotos: [String] = []
    
    var addDetailButton: UIButton!
    
    var photoDocumentsCell: AddPhotoTableViewCell?
    var photoBillCell: AddPhotoTableViewCell?
    var photoDevCell: AddPhotoTableViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        
        idReport = FirebaseMethods.generateReportId()
    }
    
    func commonInit(orderId: String, orderNumber: String, orderDescription: String, orderAddress: Address, timeOfWork: Int, clientName: String, userName: String) {
        self.orderId = orderId
        self.orderNumber = orderNumber
        self.orderDescription = orderDescription
        self.orderAddress = orderAddress
        self.timeOfWork = timeOfWork
        
        self.clientName = clientName
        self.userName = userName
    }
    func parsePhotoNames() -> ([String], [String], [String]) {
        guard var photoDocumentsItems = photoDocumentsCell?.names,
            var photoDevItems = self.photoDevCell?.names,
            var photoBillItems = self.photoBillCell?.names else {
                return ([""], [""], [""])
        }
        
        photoDocumentsItems.remove(at: 0)
        photoDevItems.remove(at: 0)
        photoBillItems.remove(at: 0)
        return (photoDocumentsItems, photoDevItems, photoBillItems)
    }
    func checkFields() {
        var check = [4, 5, 12]
        for i in 0..<detailItems.count {
            check.append((i + 1) * 100)
            check.append((i + 1) * 100 + 1)
            check.append((i + 1) * 100 + 2)
        }
        
        var pleaseFillField: [Int] = []
        let arrayOfKeys = Array(dataToSend.keys)
        for item in check {
            if arrayOfKeys.contains(item) {
                if dataToSend[item]!.isEmpty {
                    pleaseFillField.append(item)
                }
            }
            else {
                pleaseFillField.append(item)
            }
        }
        if let count = photoDocumentsCell?.items.count, count < 2 {
            pleaseFillField.append(-1) // for photo document
        }
        if let count = photoDevCell?.items.count, count < 2 {
            pleaseFillField.append(-2) // for photo technics
        }
        
        if !(pleaseFillField.isEmpty) {
            var emptyFields = "Необходимо заполнить следующие поля:\n"
            for item in pleaseFillField {
                switch item {
                case 4:
                    emptyFields += "\"Номер сохранной расписки\""
                case 5:
                    emptyFields += "\"Сумма предоплаты\""
                case 12:
                    emptyFields += "\"Комментарий\""
                case -1:
                    emptyFields += "\"Фото документов\""
                case -2:
                    emptyFields += "\"Фото техники\""
                case let x where (x >= 100) && (x % 10 == 0):
                    emptyFields += "\"Название деталей\" в детали №\(x / 100)"
                case let x where (x >= 100) && (x % 10 == 1):
                    emptyFields += "\"Стоимость деталей\" в детали №\(x / 100)"
                case let x where (x >= 100) && (x % 10 == 2):
                    emptyFields += "\"Количество деталей\" в детали №\(x / 100)"
                default:
                    continue
                }
                emptyFields += "\n"
            }
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: emptyFields, okTitle: Constants.okString)
            return
        }
    }
    
    func sendReport() {
        view.endEditing(true)
        checkFields()
        
        // TODO: send prepayment to base
        guard let bsoNumber = dataToSend[4], !bsoNumber.isEmpty else {return}
        guard let prepayment = dataToSend[5], !prepayment.isEmpty else {return}
        guard let comment = dataToSend[12], !comment.isEmpty else {return}
        
        guard comment.count < 254 else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCount, okTitle: Constants.okString)
            return
        }
        if !(bsoNumber.isConform(to: "[^0-9 ]") &&
            prepayment.isConform(to: "[^0-9 ]") &&
            comment.isConform(to: "[^a-zA-Zа-яА-ЯёЁ()0-9 .,-?!;:+%]")) {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
        }
        guard bsoNumber.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongInt, okTitle: Constants.okString)
                return
        }
        guard prepayment.count < 8 else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCost, okTitle: Constants.okString)
            return
        }
        guard let prepaymentDouble = Double(prepayment) else {return}
        var details: [UsedDetails] = []
        for i in 0..<detailItems.count {
            guard let detailName = dataToSend[(i + 1) * 100], !detailName.isEmpty else {return}
            guard let detailCount = dataToSend[(i + 1) * 100 + 1], !detailCount.isEmpty else {return}
            guard let detailPrice = dataToSend[(i + 1) * 100 + 2], !detailPrice.isEmpty else {return}
            
            if !(detailName.isConform(to: "[^a-zA-Z0-9а-яА-ЯёЁ .,-:+]") &&
                detailCount.isConform(to: "[^0-9 ]") &&
                detailPrice.isConform(to: "[^0-9,. ]")) {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
            }
            guard detailName.count < 254 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCount, okTitle: Constants.okString)
                return
            }
            guard detailCount.count < 11 else {
                    ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongInt, okTitle: Constants.okString)
                    return
            }
            guard detailPrice.count < 8 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongCost, okTitle: Constants.okString)
                return
            }
            guard let count = Int(detailCount) else { return }
            guard let price = Double(detailPrice) else {return}
            details.append(UsedDetails(name: detailName, price: price, quantity: count))
        }
        
        let (photoDocumentsItems, photoDevItems, photoBillItems) = parsePhotoNames()
        guard photoDocumentsItems.count < 11,
            photoDevItems.count < 11,
            photoBillItems.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongPhotos, okTitle: Constants.okString)
                return
        }
        allPhotos = photoDocumentsItems + photoDevItems + photoBillItems
        
        let idReportToSend = self.idReport ?? "NULL_ID_REPORT_\(Date())"
        
        //        let imagesWithNames = initImageArray(documents: photoDocumentsItems, bills: photoDevItems, devs: photoBillItems, idReport: idReportToSend)
        
        let reportModernization = Report(bsoNumber: bsoNumber, comments: comment, cost: prepaymentDouble, agreementDate: nil, defects: nil, agreementNumber: nil, orderId: orderId, reportIsAccepted: false, status: .modernization, timeSpend: timeOfWork, productType: nil, usedDetails: details, userId: FirebaseMethods.getUID(), cancelOrDeclineReason: nil, clientName: clientName, userName: userName,  orderNumber: orderNumber, orderDescription: orderDescription, orderAddress: orderAddress, date: Date())
        areYouSureToSendReportAlert(vc: self, idChecker: orderId) {
            
            self.addActivityIndicator()
            
            self.addProgressView()
            PostService.create(for: self.allPhotos, progressView: self.progressView) { (check) in
                if check {
                    FirebaseMethods.writeToReports(id: idReportToSend, report: reportModernization, finish: { (lineString) in
                        print(lineString)
                        guard let id = self.orderId else {
                            print("[sendReport] error in getting id or order")
                            return }
                        FirebaseMethods.updateOrder(dictionary: ["status":"modernization"], documentId: id)
                        self.activityIndicator.removeFromSuperview()
                       // self.progressView.removeFromSuperview()
                        self.alert.dismiss(animated: true, completion: nil)
                        self.popToMapViewController()
                    })
                } else {
                    self.failureUploading()
                }
            }
        }
    }
    
   
    
    
    func addActivityIndicator() {
        view.addSubview(self.activityIndicator)
        activityIndicator.frame = self.view.bounds
        activityIndicator.startAnimating()
    }
    func addProgressView() {
        progressView.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
        progressView.trackTintColor = .lightGray
        progressView.tintColor = .blue
        alert.view.addSubview(progressView)
        present(alert, animated: true)
    }
    func failureUploading() {
        activityIndicator.removeFromSuperview()
        progressView.removeFromSuperview()
        ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.canNotLoadPhoto, message: Constants.tryAgain, okTitle: Constants.okString)
    }
    deinit {
        print("[ModernizationViewController] deinit")
        
        if !allPhotos.isEmpty{
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        } else {
            let (photoDocumentsItems, photoDevItems, photoBillItems) = parsePhotoNames()
            allPhotos = photoDocumentsItems + photoDevItems + photoBillItems
            allPhotos.forEach { ImageStore.delete(imageNamed: $0) }
        }
    }
}

extension ModernizationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return detailItems.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = UITableViewCell()
            cell.commonInit(text: "Детали:", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: false)
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailTableViewCell") as? DetailTableViewCell else {return UITableViewCell()}
            cell.delegate = self
            cell.countDetailTextField.keyboardType = .numberPad
            cell.detailPriceTextField.keyboardType = .numberPad

            detailItems[indexPath.row].nameTextField  = cell.detailNameTextField
            detailItems[indexPath.row].countTextField = cell.countDetailTextField
            detailItems[indexPath.row].priceTextField = cell.detailPriceTextField

            detailItems[indexPath.row].nameTextField?.delegate  = self
            detailItems[indexPath.row].countTextField?.delegate = self
            detailItems[indexPath.row].priceTextField?.delegate = self
            
            detailItems[indexPath.row].nameTextField?.text  = dataToSend[(indexPath.row + 1) * 100]
            detailItems[indexPath.row].countTextField?.text = dataToSend[(indexPath.row + 1) * 100 + 1]
            detailItems[indexPath.row].priceTextField?.text = dataToSend[(indexPath.row + 1) * 100 + 2]
            
            detailItems[indexPath.row].nameTextField?.tag  = (indexPath.row + 1) * 100
            detailItems[indexPath.row].countTextField?.tag = (indexPath.row + 1) * 100 + 1
            detailItems[indexPath.row].priceTextField?.tag = (indexPath.row + 1) * 100 + 2

            

            cell.tableView = tableView
            return cell

        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportButtonTableViewCell") as? ReportButtonTableViewCell else {return UITableViewCell()}
            cell.delegate = self
            cell.tableView = tableView
            cell.button.setTitle("Добавить еще", for: .normal)
            return cell
            
        case 3:
            let cell = UITableViewCell()
            cell.commonInit(text: "Информация * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Номер сохранной расписки"
            cell.textField.keyboardType = .numberPad
            
            bsoNumberTextField = cell.textField
            bsoNumberTextField.delegate = self
            bsoNumberTextField.tag = indexPath.section
            bsoNumberTextField.text = dataToSend[indexPath.section]
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportTextFieldTableViewCell") as? ReportTextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = "Сумма предоплаты"
            cell.textField.keyboardType = .numberPad
            
            prepaymentTextField = cell.textField
            prepaymentTextField.delegate = self
            prepaymentTextField.tag = indexPath.section
            prepaymentTextField.text = dataToSend[indexPath.section]
            return cell
            
        case 6:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото документов * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 8:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото техники * :", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: true)
            return cell
            
        case 10:
            let cell = UITableViewCell()
            cell.commonInit(text: "Фото чека:", textColor: #colorLiteral(red: 0.1232355013, green: 0.5269755125, blue: 0.8301456571, alpha: 1), textSize: 18, isRequired: false)
            return cell
            
        case 7:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_doc")
            photoDocumentsCell = cell
            return cell

        case 9:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_dev")
           
            photoDevCell = cell
            return cell

        case 11:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "addPhotoCell") as? AddPhotoTableViewCell else {return UITableViewCell()}
            guard let navigator = navigationController else {return UITableViewCell()}
            cell.commonInit(viewController: self, navigator: navigator, type: "\(idReport ?? "NULL_ID_REPORT_\(Date())")_bill")
            photoBillCell = cell
            return cell
            
        case 12:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "commentTableViewCell") as? CommentTableViewCell else {return UITableViewCell()}
            cell.comment.text = ""
            cell.tableView = tableView
            cell.delegate = self
            commentTextField = cell.comment
            commentTextField.delegate = self
            commentTextField.tag = indexPath.section
            commentTextField.text = dataToSend[indexPath.section]
            return cell
            
        case 13:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reportButtonTableViewCell") as? ReportButtonTableViewCell else {return UITableViewCell()}
            cell.delegate = self
            cell.tableView = tableView
            cell.button.setTitle("Модернизация", for: .normal)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    @objc func addDetailItem() {
        detailItems.append(DetailItem(detailName: "", detailCount: "", detailPrice: ""))
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: detailItems.count - 1, section: 1)], with: .fade)
        tableView.endUpdates()
    }
    
}

// MARK: - text field delegate
extension ModernizationViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        dataToSend[textField.tag] = text
        print(text + " of \(textField.tag) tag")
    }
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if textField.tag > 100 && textField.tag % 100 == 2 {
            let countdots = (textField.text ?? "").components(separatedBy: ",").count - 1
            
            if countdots > 0 && string == ","
            {
                return false
            }
        }
        return true
    }
}

// MARK: - text view delegate
extension ModernizationViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let text = textView.text else {return}
        dataToSend[textView.tag] = text
    }
    
    
}

// MARK: - detail cell delegate
extension ModernizationViewController: DetailCellDelegate {
    func didTapDeleteItem(_ tableView: UITableView, _ indexPath: IndexPath) {
        view.endEditing(true)
        
        for index in indexPath.row..<detailItems.count {
            dataToSend[(index + 1) * 100] = dataToSend[(index + 2) * 100]
            dataToSend[(index + 1) * 100 + 1] = dataToSend[(index + 2) * 100 + 1]
            dataToSend[(index + 1) * 100 + 2] = dataToSend[(index + 2) * 100 + 2]
        }
        detailItems.remove(at: indexPath.row)
        
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }
}

extension ModernizationViewController: SendButtonDelegate {
    func didButtonTapped(_ tableView: UITableView, _ indexPath: IndexPath) {
        if indexPath.section == 2 {
            addDetailItem()
        }
        if indexPath.section == 13 {
            sendReport()
        }
    }
}

extension ModernizationViewController: AutoFillDelegate {
    
    func pushToAutoFillViewController() {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "refuseReportVC") as? RefuseReportViewController else {return}
        guard let navigator = navigationController else {return}
        vc.allowMultipleSelection = true
        vc.modernizationVC = self
        navigator.pushViewController(vc, animated: true)
    }
    
    func didTappedAutoFill(_ tableView: UITableView, _ indexPath: IndexPath) {
        pushToAutoFillViewController()
    }
}
