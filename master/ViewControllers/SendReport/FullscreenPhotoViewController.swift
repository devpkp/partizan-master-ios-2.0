//
//  FullscreenPhotoViewController.swift
//  master
//
//  Created by Polina Guryeva on 01/10/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class FullscreenPhotoViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var image: UIImage!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
    }
   
    @IBAction func closeButtonTapped(_ sender: Any) {
        guard let navigator = navigationController else {return}
        imageView = nil
        image = nil
        navigator.popViewController(animated: true)
    }
    
    deinit {
        print("FullscreenPhotoViewController deinit")
    }
    
}
