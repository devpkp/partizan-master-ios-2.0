//
//  SuperSendReportViewController.swift
//  master
//
//  Created by Polina Guryeva on 13.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation
import UIKit

class SuperSendReportViewController: UIViewController {
    
    lazy var backButton: UIBarButtonItem = {
        let button = UIBarButtonItem()
        button.title = "Назад"
        return button
    }()
    
    
    
    func popToMapViewController() {
        guard let navigator = navigationController else {return}
        print(navigator.viewControllers)
        for vc in navigator.viewControllers {
            if let mapVC = vc as? MapViewController {
                navigator.popToViewController(mapVC, animated: true)
                mapVC.completeSendViewAnimation()
            }
        }
    }
    
    func    areYouSureToSendReportAlert(vc: UIViewController, idChecker: String?, closure: @escaping () -> ()) {
        weak var unsafeViewController = vc
        guard let safeViewController = unsafeViewController else { return }
        let alert = UIAlertController(title: Constants.attensionAlertMessage, message: Constants.areYouSureToSendReport, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.cancel, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.send, style: .default, handler: {  (_) in
            guard let id = idChecker else {
                print("[areYouSureToSendReportAlert] no id order")
                return
            }
            FirebaseMethods.checkValidOrder(id) { isValid in
                if !isValid {
                    ShowAlerts.showAlert(viewControllerToShow: safeViewController, headerTitle: Constants.attensionAlertMessage, message: Constants.checkCurrentOrder, okTitle: Constants.okString)
                    return
                }
                guard let onlineStatus = user?.online,
                    onlineStatus == true else {
                        ShowAlerts.showAlert(viewControllerToShow: safeViewController, headerTitle: Constants.cancel, message: Constants.checkOnlineStatus, okTitle: Constants.okString)
                        return
                }
                FirebaseMethods.updateLocationUser(event: LocationEventStatus.reportCreated.rawValue, location: globalLocation, orderId: id, userId: FirebaseMethods.getUID())
                closure()
            }
        }))
        safeViewController.present(alert, animated: true)
    }
}
