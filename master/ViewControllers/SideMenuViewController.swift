//
//  SideMenuViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 16.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import Firebase
import SideMenu

class SideMenuViewController: UIViewController {
    
    var menuItems = [MenuItem(icon: UIImage(), label: ""),
                     MenuItem(icon: #imageLiteral(resourceName: "Zayavki"), label: "карта"),
                     MenuItem(icon: #imageLiteral(resourceName: "active_zayavki"), label: "в работе"),
                     MenuItem(icon: #imageLiteral(resourceName: "reports"), label: "отчеты"),
                     MenuItem(icon: #imageLiteral(resourceName: "Statistics"), label: "статистика"),
                  //   MenuItem(icon: #imageLiteral(resourceName: "Settings"), label: "настройки"),
                     MenuItem(icon: #imageLiteral(resourceName: "Exit"), label: "выход")]
    var menuIsOpen = false
    var statusIsOnline = false
    
    var colorOfStatus: UIView?
    var requestCount = 0
    
    @IBOutlet weak var menuTableView: UITableView!
    private var listener: ListenerRegistration?
    
    @objc func changeRequest(notification: NSNotification) {
        requestCount = activeRequestCount
        menuTableView.reloadData()
    }
    
    func listenerStatusOnline() {
        let userID = FirebaseMethods.getUID()
        let queryListenerOnlineStatus = FirebaseMethods.db.collection("users").document(userID)
        listener = queryListenerOnlineStatus.addSnapshotListener { documentSnapshot, error in
            guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            guard let data = document.data() else {
                print("Document data was empty.")
                return
            }
            
            self.statusIsOnline = data["online"] as? Bool ?? false
            user = User(dictionary: data)
            guard let deviceId = UIDevice.current.identifierForVendor?.uuidString else {
                print("[Auth.auth().createUser] error getting deviceId")
                return
            }
            if deviceId != user?.deviceId {
                
                //ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: "", message: Constants.anotherAuth, okTitle: Constants.okString)
                self.logOutApplication(changeStatus: false)
            }
            self.changeStatusOfUser()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.separatorStyle = .none
        menuTableView.rowHeight = UITableView.automaticDimension
        menuTableView.estimatedRowHeight = 70.0
        listenerStatusOnline()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeRequest(notification:)),
                                               name: .changeRequest,
                                               object: nil)
    }
    
    func changeStatusOfUser() {
        if statusIsOnline {
            colorOfStatus?.backgroundColor = #colorLiteral(red: 0.5450980392, green: 0.7628648281, blue: 0.2914263904, alpha: 1)
        } else {
            colorOfStatus?.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        }
    }
    func logOutApplication(changeStatus: Bool){
        FirebaseMethods.signOut(changeStatus: changeStatus)
        guard let navigator = navigationController else {return}
        navigator.viewControllers.removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        if let navigatorToExit = appDelegate.exitViewController {
            for controller in navigatorToExit.viewControllers as Array {
                if controller.isKind(of: AuthorizationViewController.self) {
                    appDelegate.exitViewController = nil
                    print(navigatorToExit.viewControllers)
                    navigatorToExit.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func updateOnlineStatus() {
        self.statusIsOnline = !self.statusIsOnline
        FirebaseMethods.changeFieldsOfUser(["online": self.statusIsOnline])
        let locationStatus = self.statusIsOnline ? LocationEventStatus.online.rawValue : LocationEventStatus.offline.rawValue
        FirebaseMethods.updateLocationUser(event: locationStatus, location: globalLocation, orderId: nil, userId: FirebaseMethods.getUID())
        menuTableView.reloadData()
    }
    deinit {
        listener?.remove()
        NotificationCenter.default.removeObserver(self,
                                                  name: .changeRequest,
                                                  object: nil)
    }
}

// MARK: - table view data source
extension SideMenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "statusCell") as? StatusTableViewCell else {return UITableViewCell()}
            cell.avatar.image = #imageLiteral(resourceName: "Zayavki")
            colorOfStatus = cell.statusView
            changeStatusOfUser()
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell") as? MenuTableViewCell else {return UITableViewCell()}
            
            if indexPath.row == 2 { // active orders
                cell.activeRequestCountLabel.text = " \(requestCount) "
            } else {
                cell.activeRequestCountLabel.isHidden = true
            }
            cell.icon.image = menuItems[indexPath.row].icon
            cell.label.text = menuItems[indexPath.row].label
            return cell
        }
    }
    
}

// MARK: - table view delegate
extension SideMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let navigator = navigationController else {return}
        
        switch indexPath.row {
        case 0:
            if canChangeOnlineStatus || !statusIsOnline {
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                if statusIsOnline {
                    alert.message = Constants.fromWork
                } else {
                    alert.message = Constants.toWork
                }
                alert.addAction(UIAlertAction(title: Constants.no, style: .destructive, handler: nil))
                alert.addAction(UIAlertAction(title: Constants.yes, style: .default, handler: { [weak self] (_) in
                   self?.updateOnlineStatus()
                }))
                present(alert, animated: true)
            } else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: "", message: Constants.youCanNotGoAway, okTitle: Constants.okString)
            }
        case 1:
            guard let mapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapViewController") as? MapViewController else {return}
            navigator.pushViewController(mapVC, animated: false)
        case 2:
            guard let activesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "activeRequestVC") as? ActiveViewController else {return}
            navigator.pushViewController(activesVC, animated: false)
        case 3:
            guard let reportsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "reportListVC") as? ReportListViewController else {return}
            navigator.pushViewController(reportsVC, animated: false)
        case 4:
            guard let reportsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "statisticVC") as? ReportsViewController else {return}
            navigator.pushViewController(reportsVC, animated: false)
//        case 5:
//            guard let settingsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingsViewController") as? SettingsViewController else {return}
//            navigator.pushViewController(settingsVC, animated: false)
        case 5:
            if !canChangeOnlineStatus {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: "", message: Constants.youCanNotGoAway, okTitle: Constants.okString)
            } else {
                navigator.dismiss(animated: true, completion: nil)
                logOutApplication(changeStatus: true)
            }
        default:
            print("default case: error!")
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
