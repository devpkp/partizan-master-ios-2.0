//
//  ViewController.swift
//  partizanMaster
//
//  Created by Иван Кулчеев on 11.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SideMenu

class AuthorizationViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    let launchScreen = UIImageView(image: #imageLiteral(resourceName: "authWait"))
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    var documents: [documentDictionary] = []

    @IBOutlet weak var loginEmailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBAction func registrationButtonTapped(_ sender: Any) {
        guard let registrationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "registrationViewController") as? RegistrationViewController else {return}
        guard let navigator = navigationController else {return}
        navigator.pushViewController(registrationVC, animated: true)
    }

    @IBAction func recoveryPasswordButtonTapped(_ sender: Any) {
        guard let passwordRecoveryVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "passwordRecoveryViewController") as? PasswordRecoveryViewController else {return}
        guard let navigator = navigationController else {return}
        navigator.pushViewController(passwordRecoveryVC, animated: true)
    }
    func signingIn() {
        guard let email = loginEmailTextField.text, !email.isEmpty else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: "Поле \"Электронная почта\" пустое", okTitle: Constants.okString)
            return
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: "Поле \"Пароль\" пустое", okTitle: Constants.okString)
            return
        }
        addActivityIndicator()
        Auth.auth().signIn(withEmail: email, password: password) { (_, error) in
            if error != nil {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAuth, message: Constants.enterCorrectData, okTitle: Constants.okString)
            } else {
                print("User in the system")
            }
            self.removeActivityIndicator()
        }
    }
    @IBAction func signInButtonTapped(_ sender: Any) {
       signingIn()
    }
    func addLaunchScreen() {
        launchScreen.frame = view.bounds
        view.addSubview(launchScreen)
    }
    func removeLaunchScreen() {
        launchScreen.removeFromSuperview()
    }
    
    func addActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
    }

    func removeActivityIndicator() {
        activityIndicator.removeFromSuperview()
    }

    func pushMapController() {
        guard let mapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapViewController") as? MapViewController else {return}
        guard let navigator = self.navigationController else {return}
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.exitViewController = navigator
        navigator.pushViewController(mapVC, animated: false)
    }

    func checkLoggedIn() {
        Auth.auth().addStateDidChangeListener { _, user in
            if user != nil {
                print("[checkLoggedIn] User was logined")
                FirebaseMethods.authActions(error: nil)
                self.pushMapController()
            } else {
                self.removeActivityIndicator()
                self.removeLaunchScreen()
                print("[checkLoggedIn]  No loginedIn")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginEmailTextField.delegate = self
        passwordTextField.delegate = self

        addLaunchScreen()
        addActivityIndicator()
        self.checkLoggedIn()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeActivityIndicator()
        removeLaunchScreen()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

// MARK: - text field delegate
extension AuthorizationViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if nextTag == passwordTextField.tag {
            passwordTextField.becomeFirstResponder()
        } else {
            //textField.resignFirstResponder()
            signingIn()
        }
        return false
    }

}
