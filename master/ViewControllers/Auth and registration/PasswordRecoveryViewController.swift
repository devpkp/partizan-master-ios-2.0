//
//  PasswordRecoveryViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 13.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import FirebaseAuth

class PasswordRecoveryViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet var completeSendView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func viewInit() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        navigationController?.navigationBar.tintColor = .white
        navigationController?.setNavigationBarHidden(false, animated: true)

        completeSendView.frame = CGRect(x: 0.0, y: -100.0, width: view.frame.size.width, height: 70.0)
    }
    func animateView() {
        guard let currVC = navigationController?.topViewController else {return}
        currVC.view.addSubview(completeSendView)
        UIView.animate(withDuration: 0.6) {
            self.completeSendView.frame.origin.y = currVC.view.frame.origin.y
        }
        UIView.animate(withDuration: 0.4, delay: 2.1, options: .beginFromCurrentState, animations: {
            self.completeSendView.frame.origin.y -= 300.0
        }, completion: nil)

    }
    @IBAction func sendButtonTapped(_ sender: Any) {
        guard let email = emailTextField.text else {return}
        if !email.isConform(to: "[^A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}") {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
        }

        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if error != nil {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.somethingWrong, okTitle: Constants.okString)
                
            } else {
                print("[sendPasswordReset] recovery e-mail")
                
                self.navigationController?.popViewController(animated: true)
                self.animateView()
            }

        }
    }
}
