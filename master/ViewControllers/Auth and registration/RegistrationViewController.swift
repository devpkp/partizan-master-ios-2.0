//
//  RegistrationViewController.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 12.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import JMMaskTextField_Swift

class RegistrationViewController: UIViewController {

    @IBOutlet weak var textFieldsTableView: UITableView!
    private var currentTextField: UITextField?

    
    let placeholders = ["Фамилия", "Имя", "Отчество", "Телефон", "Электронная почта", "Пароль", "Повторите пароль"]
  //  let skills = ["Навык 1", "Навык 2", "Навык 3", "Навык 4"]
    let skills: [String] = []
    var dataToSend: [Int:String] = [:]

    var emailTextField: UITextField!
    var passTextField: UITextField!
    var repeatPassTextField: UITextField!
    var secondNameTextField: UITextField!
    var firstNameTextField: UITextField!
    var patronymicTextField: UITextField!
    var phoneNumberTextField: JMMaskTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldsTableView.dataSource = self
        textFieldsTableView.delegate = self
        
        textFieldsTableView.rowHeight = UITableView.automaticDimension
        textFieldsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        viewInit()
    }

    func viewInit() {
        textFieldsTableView.separatorStyle = .none
        textFieldsTableView.allowsSelection = false

        let backButton = UIBarButtonItem()
        backButton.title = "Назад"
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        navigationController?.navigationBar.tintColor = .white
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @objc func textFieldDidChanged(textField: UITextField) {
        guard let text = textField.text else {return}

        if text.count >= 1 {
            switch textField {
            case secondNameTextField:
                firstNameTextField.becomeFirstResponder()
            case firstNameTextField:
                patronymicTextField.becomeFirstResponder()
            case patronymicTextField:
                phoneNumberTextField.becomeFirstResponder()
            case phoneNumberTextField:
                emailTextField.becomeFirstResponder()
            case emailTextField:
                passTextField.becomeFirstResponder()
            default:
                return
            }
        }
    }

    @objc func buttonPressed() {
        view.endEditing(true)

        //print(dataToSend)
        guard let secondName = dataToSend[1], !secondName.isEmpty,
            let firstName = dataToSend[2], !firstName.isEmpty,
            let patronymic = dataToSend[3], !patronymic.isEmpty,
            var phoneNumber = dataToSend[4], !phoneNumber.isEmpty,
            let email = dataToSend[5], !email.isEmpty,
            let password = dataToSend[6], !password.isEmpty,
            let repeatPassword = dataToSend[7], !repeatPassword.isEmpty else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.enterAllFields, okTitle: Constants.okString)
                return
        }
        
        if !(secondName.isConform(to: "[^a-zA-Zа-яА-ЯёЁ-]") &&
            firstName.isConform(to: "[^a-zA-Zа-яА-ЯёЁ-]") &&
            patronymic.isConform(to: "[^a-zA-Zа-яА-ЯёЁ-]") &&
            phoneNumber.isConform(to: "[^0-9()+- ]") &&
            email.isConform(to: "[^A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")) {
            
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.inputForbiddenSymbols, okTitle: Constants.okString)
            return
        }
        phoneNumber = phoneNumber.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        
        if password != repeatPassword {
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAlertMessage, message: Constants.passwordsIsNotSimilar, okTitle: Constants.okString)
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            guard let email = authResult?.user.email, error == nil else {
                ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.errorAuth, message: Constants.enterCorrectData, okTitle: Constants.okString)
                return
            }
//            guard let deviceId = UIDevice.current.identifierForVendor?.uuidString else {
//                print("[Auth.auth().createUser] error getting deviceId")
//                return
//            }
            let user = User(email: email, firstName: firstName, lastName: secondName, middleName: patronymic, online: false, phoneNumber: phoneNumber, deviceId: "")
            FirebaseMethods.writeToUsers(user: user)
            FirebaseMethods.authActions(error: nil)
            ShowAlerts.showAlert(viewControllerToShow: self, headerTitle: Constants.completeRegistration, message: "\(email) зарегистрирован", okTitle: Constants.okString)
        }

    }
}

// MARK: - table view data source
extension RegistrationViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeholders.count + skills.count + 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = UITableViewCell()
            cell.textLabel?.text = "Введите личные данные:"
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell.textLabel?.textColor = #colorLiteral(red: 0.1208477691, green: 0.1451228857, blue: 0.1494033635, alpha: 1)
            return cell
        case 1, 2, 3, 4, 5, 6, 7:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell") as? TextFieldTableViewCell else {return UITableViewCell()}
            cell.textField.placeholder = placeholders[indexPath.row - 1]
            if indexPath.row == 1 {
                secondNameTextField = cell.textField
                secondNameTextField.tag = indexPath.row
                secondNameTextField.delegate = self
                secondNameTextField.text = dataToSend[indexPath.row]

                cell.textField.autocapitalizationType = .words
            }
            if indexPath.row == 2 {
                firstNameTextField = cell.textField
                firstNameTextField.tag = indexPath.row
                firstNameTextField.delegate = self
                firstNameTextField.text = dataToSend[indexPath.row]

                cell.textField.autocapitalizationType = .words
            }
            if indexPath.row == 3 {
                patronymicTextField = cell.textField
                patronymicTextField.tag = indexPath.row
                patronymicTextField.delegate = self
                patronymicTextField.text = dataToSend[indexPath.row]

                cell.textField.autocapitalizationType = .words
            }
            if indexPath.row == 4 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "maskField") as? MaskFieldTableViewCell else {return UITableViewCell()}
                phoneNumberTextField = cell.maskField
                phoneNumberTextField.tag = indexPath.row
                phoneNumberTextField.delegate = self
                phoneNumberTextField.text = dataToSend[indexPath.row]
               // phoneNumberTextField.maskString = "+7(000)000-00-00"
             //   phoneNumberTextField.tintColor = UIColor.clear
                cell.maskField.keyboardType = .phonePad
                return cell
            }
            if indexPath.row == 5 {
                emailTextField = cell.textField
                emailTextField?.tag = indexPath.row
                emailTextField?.delegate = self
                emailTextField?.text = dataToSend[indexPath.row]

                cell.textField.keyboardType = .emailAddress
            }
            if indexPath.row == 6 {
                passTextField = cell.textField
                passTextField?.tag = indexPath.row
                passTextField?.delegate = self
                passTextField?.text = dataToSend[indexPath.row]

                cell.textField.isSecureTextEntry = true
            }
            if indexPath.row == 7 {
                repeatPassTextField = cell.textField
                repeatPassTextField?.tag = indexPath.row
                repeatPassTextField?.delegate = self
                repeatPassTextField?.text = dataToSend[indexPath.row]
                
                cell.textField.isSecureTextEntry = true
            }
            return cell
//        case 8:
//            let cell = UITableViewCell()
//            cell.textLabel?.text = "Навыки:"
//            cell.textLabel?.textAlignment = .center
//            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
//            cell.textLabel?.textColor = #colorLiteral(red: 0.1208477691, green: 0.1451228857, blue: 0.1494033635, alpha: 1)
//            return cell
//        case 9, 10, 11, 12:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "checkBoxCell") as? CheckBoxTableViewCell else {return UITableViewCell()}
//            cell.pointLabel.text = skills[indexPath.row - placeholders.count - 2]
//            return cell
        case 8:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell") as? ButtonTableViewCell else {return UITableViewCell()}
            cell.tableButton?.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            return cell
        default:
            print("error: default case!")
            return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        }
    }

}

// MARK: - text field delegate
extension RegistrationViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField = textField
        guard let text = textField.text else { return }
        dataToSend[textField.tag] = text
        print(text + " of \(textField.tag) tag")
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        switch nextTag {
        case firstNameTextField.tag:
            firstNameTextField.becomeFirstResponder()
        case patronymicTextField.tag:
            patronymicTextField.becomeFirstResponder()
        case phoneNumberTextField.tag:
            phoneNumberTextField.becomeFirstResponder()
        case emailTextField.tag:
            emailTextField.becomeFirstResponder()
        case passTextField.tag:
            passTextField.becomeFirstResponder()
        case repeatPassTextField.tag:
            repeatPassTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }

}
