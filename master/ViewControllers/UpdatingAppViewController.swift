//
//  UpdatingAppViewController.swift
//  master
//
//  Created by Sergey Gusev on 26/11/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class UpdatingAppViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }


}
