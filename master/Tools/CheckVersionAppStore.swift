//
//  CheckVersionAppStore.swift
//  master
//
//  Created by Sergey Gusev on 15/11/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation

class CheckVersionAppStore {
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    static func isUpdateAvailable(completion: @escaping (Bool?, Int?, Error?) -> Void) throws -> URLSessionDataTask {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any],
                    let version = result["version"] as? String,
                let id = result["trackId"] as? Int else {
                    throw VersionError.invalidResponse
                }
                completion(version != currentVersion, id, nil)
            } catch {
                completion(nil, nil, error)
            }
        }
        task.resume()
        return task
    }
}
