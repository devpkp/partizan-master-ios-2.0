//
//  CheckTextField.swift
//  master
//
//  Created by Polina Guryeva on 19.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func removeCharacters(from forbiddenChars: CharacterSet) -> String {
        let passed = self.unicodeScalars.filter { !forbiddenChars.contains($0) }
        return String(String.UnicodeScalarView(passed))
    }
    
    func removeCharacters(from: String) -> String {
        return removeCharacters(from: CharacterSet(charactersIn: from))
    }
    
    func isConform(to mask: String) -> Bool {
        return !isEmpty && range(of: mask, options: .regularExpression) == nil
    }
}

extension UITextField {
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}

extension Date {
    func startOfMonth() -> Date? {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        var dateComponents = DateComponents()
        dateComponents.year = components.year
        dateComponents.month = components.month
        dateComponents.day = 1
        dateComponents.timeZone = TimeZone(abbreviation: "UTC")
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 1
        
        // Create date from components
        let userCalendar = Calendar.current // user calendar
        
        guard let someDateTime = userCalendar.date(from: dateComponents) else { return Date()}
        return someDateTime
    }
    
    
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
    
    
    
}

extension UITableViewCell {
    
    func commonInit(text: String?, textColor: UIColor?, textSize: CGFloat, isRequired: Bool) {
        guard let textInLabel = text else {return}
        if isRequired {
            let mutableString = NSMutableAttributedString(string: textInLabel)
            mutableString.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5534581218, green: 0, blue: 0.1684132761, alpha: 1)], range: NSRange(location:textInLabel.count - 3,length: 1))
            self.textLabel?.attributedText = mutableString
        } else {
            self.textLabel?.text = text
        }
        self.textLabel?.textColor = textColor
        self.textLabel?.font = UIFont.boldSystemFont(ofSize: textSize)
    }
    
}

extension UIImage {
    func scale() -> UIImage {
        let data = self.jpegData(compressionQuality: 0.5)
        
        let image = UIImage(data: data!)
        return image!
    }
    
//    func resizeImage(targetSize: CGSize) -> UIImage {
//        let size = self.size
//
//        let widthRatio  = targetSize.width  / size.width
//        let heightRatio = targetSize.height / size.height
//
//        // Figure out what our orientation is, and use that to form the rectangle
//        var newSize: CGSize
//        if(widthRatio > heightRatio) {
//            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
//        } else {
//            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
//        }
//
//        // This is the rect that we've calculated out and this is what is actually used below
//        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
//
//        // Actually do the resizing to the rect using the ImageContext stuff
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
//        self.draw(in: rect)
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        return newImage!
//    }
    public func resizeImage(targetSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(targetSize)
        self.draw(in: CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage!
    }
}

struct ImageStore {
    
    static func delete(imageNamed name: String) {
        guard let imagePath = ImageStore.path(for: name) else {
            return
        }
        try? FileManager.default.removeItem(at: imagePath)
    }
    
    static func retrieve(imageNamed name: String) -> UIImage? {
        guard let imagePath = ImageStore.path(for: name) else {
            return nil
        }
        
        return UIImage(contentsOfFile: imagePath.path)
    }
    
    static func store(image: UIImage, name: String) {
        
        guard let imageData = image.jpegData(compressionQuality: 1) else {
            print("[ImageStore.store] cannot get pngData")
            return
        }
        
        guard let imagePath = ImageStore.path(for: name) else {
            print("[ImageStore.store] cannot get path")
            return
        }
        
        do {
            try imageData.write(to: imagePath)
        }
        catch {
            print("[ImageStore.store] cannot get write")
        }
    }
    
    private static func path(for imageName: String, fileExtension: String = "jpeg") -> URL? {
        let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        return directory?.appendingPathComponent("\(imageName).\(fileExtension)")
    }
}


