//
//  ShowAlerts.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 16.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit
import Foundation

class ShowAlerts {
    static func showAlert(viewControllerToShow: UIViewController, headerTitle: String, message: String, okTitle: String) {
        weak var viewController = viewControllerToShow
        let alert = UIAlertController(title: headerTitle, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: okTitle, style: UIAlertAction.Style.default, handler: nil))
        viewController?.present(alert, animated: true, completion: nil)
    }
    static func showAlertAction(viewControllerToShow: UIViewController, headerTitle: String, message: String, okTitle: String, closure: @escaping () -> ()) {
        weak var viewController = viewControllerToShow
        let alert = UIAlertController(title: headerTitle, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okTitle, style: .default, handler: {  (_) in
            closure()
        }))
        viewController?.present(alert, animated: true, completion: nil)
    }
    
}
