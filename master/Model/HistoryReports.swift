//
//  HistoryReports.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation
import Firebase

struct HistoryReports {

    let nameOfClient: String?
    let date: Date?
    let status: String?
    let nameOfMaster: String?
    let summa: Double?
    let commentOfClient: String?
    let commentOfMaster: String?
}
extension HistoryReports: DocumentSerializable {

    init?(dictionary: [String : Any?], id: String? = nil) {
        let nameOfClient = dictionary["clientName"] as? String
        let timestamp1 = dictionary["date"] as? Timestamp
        let date = timestamp1?.dateValue()
        let status = dictionary["status"] as? String
        let nameOfMaster = dictionary["userName"] as? String
        let summa = dictionary["cost"] as? Double
        let commentOfClient = dictionary["orderDescription"] as? String
        let commentOfMaster = dictionary["comments"] as? String

        self.init(nameOfClient:nameOfClient,
                  date:date,
                  status:status,
                  nameOfMaster:nameOfMaster,
                  summa:summa,
                  commentOfClient: commentOfClient,
                  commentOfMaster: commentOfMaster)
    }
}
