//
//  Detail.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 08.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
import UIKit

class DetailItem {
    var name: String
    var count: String
    var price: String

    var nameTextField: UITextField?
    var countTextField: UITextField?
    var priceTextField: UITextField?

    init(detailName: String, detailCount: String, detailPrice: String) {
        name = detailName
        count = detailCount
        price = detailPrice
    }
}
