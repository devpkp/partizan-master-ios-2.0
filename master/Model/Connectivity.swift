//
//  Connectivity.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 14.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class Connectivity {
    static let shared = Connectivity()
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    let reachabilityManager = NetworkReachabilityManager()
    
    func pushNoInternetController() {
        guard let noInternetViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "launchVC") as? LaunchViewController else {return}
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        var flag = false
        if let navigatorToExit = appDelegate.exitViewController {
            for controller in navigatorToExit.viewControllers as Array {
                if controller.isKind(of: LaunchViewController.self) {
                    flag = true
                }
            }
        }
        if !flag {
            if let navigatorToExit = appDelegate.exitViewController {
                navigatorToExit.pushViewController(noInternetViewController, animated: true)
            }
        }
    }
    
    func startNetworkReachabilityObserver() {
        reachabilityManager?.listener = { status in
            switch status {
            case .notReachable:
                print("[startNetworkReachabilityObserver] The network is not reachable")
                self.pushNoInternetController()
                
            case .unknown :
                print("[startNetworkReachabilityObserver] It is unknown whether the network is reachable")
                
            case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                print("[startNetworkReachabilityObserver] The network is reachable over the WiFi connection or the WWAN connection")
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
                
                if let navigatorToExit = appDelegate.exitViewController {
                    for controller in navigatorToExit.viewControllers as Array {
                        if controller.isKind(of: LaunchViewController.self) {
                            navigatorToExit.popViewController(animated: true)
                        }
                    }
                }
            }
        }
        reachabilityManager?.startListening()
    }
    func stopNetworkReachabilityObserver() {
        reachabilityManager?.stopListening()
    }
}
