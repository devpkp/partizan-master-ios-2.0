//
//  UserStats.swift
//  master
//
//  Created by Sergey Gusev on 13.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation
struct UserStats {
    let currentMonthStats: MonthStats?
    let previousMonthStats: MonthStats?

    var dictionary: [String: Any?] {
        return [
            "currentMonthStats": currentMonthStats?.dictionary,
            "previousMonthStats": previousMonthStats?.dictionary
        ]
    }

}
struct MonthStats {
    let total: Int?
    let canceled: Int?
    let completed: Int?
    let declined: Int?
    let earned: Double?
    let sd: Int?
    let sdPercent: Double?
    let declinedPercent: Double?

    var dictionary: [String: Any?] {
        return [
            "total": total,
            "canceled": canceled,
            "completed": completed,
            "declined": declined,
            "earned": earned,
            "sd": sd,
            "sdPercent": sdPercent,
            "declinedPercent": declinedPercent
        ]
    }
}

extension MonthStats: DocumentSerializable {

    init?(dictionary: [String : Any?], id: String? = nil) {
        let total = dictionary["total"] as? Int
        let canceled = dictionary["canceled"] as? Int
        let completed = dictionary["completed"] as? Int
        let declined = dictionary["declined"] as? Int
        let earned = dictionary["earned"] as? Double
        let sd = dictionary["sd"] as? Int
        let sdPercent = dictionary["sdPercent"] as? Double
        let declinedPercent = dictionary["declinedPercent"] as? Double

        self.init( total: total,
                   canceled: canceled,
                   completed: completed,
                   declined: declined,
                   earned: earned,
                   sd: sd,
                   sdPercent: sdPercent,
                   declinedPercent: declinedPercent)
    }
}
extension UserStats: DocumentSerializable {

    init?(dictionary: [String : Any?], id: String? = nil) {
        let dictionaryOfCurrentMonthStats = dictionary["currentMonthStats"] as? [String : Any?] ?? dictionary
        let currentMonthStats = MonthStats(dictionary: dictionaryOfCurrentMonthStats)

        let dictionaryOfPreviousMonthStats = dictionary["previousMonthStats"] as? [String : Any?] ?? dictionary
        let previousMonthStats = MonthStats(dictionary: dictionaryOfPreviousMonthStats)

        self.init(currentMonthStats: currentMonthStats,
                  previousMonthStats: previousMonthStats)
    }

}
