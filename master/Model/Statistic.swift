//
//  Statistic.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 14.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
class Statistic {
    typealias documentDictionary = [String : Any]
    let requestCount: Int?
    let completeCount: Int?
    let sdCount: Int?
    let cancelCount: Int?
    let refuseCount: Int?
    let profit: Double?

    init(statisticDictionary: documentDictionary) {
        self.requestCount = statisticDictionary["ordersCount"] as? Int
        self.completeCount = statisticDictionary["completed"] as? Int
        self.sdCount = statisticDictionary["sd"] as? Int
        self.cancelCount = statisticDictionary["canceled"] as? Int
        self.refuseCount = statisticDictionary["declined"] as? Int
        self.profit = statisticDictionary["earned"] as? Double
    }
}
