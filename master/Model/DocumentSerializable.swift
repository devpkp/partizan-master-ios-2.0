//
//  DocumentSerializable.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 16.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation

protocol DocumentSerializable {
    init?(dictionary: [String: Any?], id: String?)
}
