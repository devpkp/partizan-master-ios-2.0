//
//  User.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 17.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation

struct User {
    let email: String?
    let firstName: String?
    let lastName: String?
    let middleName: String?
    let online: Bool?
    let phoneNumber: String?
    let deviceId: String?

    var dictionary: [String: Any?] {
        return [
            "email": email,
            "firstName": firstName,
            "lastName": lastName,
            "middleName": middleName,
            "online": online,
            "phoneNumber": phoneNumber,
            "deviceId": deviceId
        ]
    }
}

extension User: DocumentSerializable {

    init?(dictionary: [String : Any?], id: String? = nil) {
        let email = dictionary["email"] as? String
        let firstName = dictionary["firstName"] as? String
        let lastName = dictionary["lastName"] as? String
        let middleName = dictionary["middleName"] as? String
        let online = dictionary["online"] as? Bool
        let phoneNumber = dictionary["phoneNumber"] as? String
        let deviceId = dictionary["deviceId"] as? String
        self.init(email: email,
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            online: online,
            phoneNumber: phoneNumber,
            deviceId: deviceId)
    }
}
