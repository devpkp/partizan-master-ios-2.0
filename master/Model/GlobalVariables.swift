//
//  GlobalVariables.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import CoreLocation

var user: User?
var globalLocation: CLLocation?

typealias documentDictionary = [String : Any]

