//
//  Geocoding.swift
//  master
//
//  Created by Sergey Gusev on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//
import GoogleMaps
import CoreLocation
import MapKit
import Alamofire
class Geocoding {
    
    static func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D,  finish: @escaping ((String, GMSCameraUpdate) -> Void)) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(googleApiKey)")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, _, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        guard let routes = json["routes"] as? NSArray else { return }
                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            
                            guard let points = dictPolyline?.object(forKey: "points") as? String else {return}
                            
                            
                            DispatchQueue.main.async {
                                //                                self.showPath(polyStr: points)
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets.init(top: 170, left: 30, bottom: 30, right: 30))
                                //                                self.mapView!.moveCamera(update)
                                finish(points, update)
                            }
                            
                        }
                    }
                } catch {
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    static func geocoding(address: Address, finish: @escaping ((CLLocationCoordinate2D?, Error?) -> Void)) {
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: geocodingApiKey) 
        let address = URLQueryItem(name: "address", value: address.addressDescriptor)
        components.queryItems = [key, address]
        
        Alamofire.request(components.url!).responseJSON { response in
            switch response.result {
            case .success:
                
                guard let json = response.result.value as? [String: Any],
                    let json1 = json["results"] as? [[String: Any]],
                    let json2 = json1.first,
                    let value = json2["geometry"] as? NSDictionary,
                    let location = value["location"] as? [String:AnyObject],
                    let latitude = location["lat"]?.doubleValue,
                    let longitude = location["lng"]?.doubleValue else {
                        print("[geocoding] no coordinates")
                        return
                }
                var coordinates = CLLocationCoordinate2D()
                coordinates.latitude = latitude
                coordinates.longitude = longitude
                finish(coordinates, nil)
                
            case .failure(let error):
                
                print(error)
                finish(nil, error)
            }
            
        }
    }
}











