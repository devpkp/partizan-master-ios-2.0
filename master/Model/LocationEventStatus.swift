//
//  LocationEventStatus.swift
//  master
//
//  Created by Иван Кулчеев on 14/11/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import Foundation
enum LocationEventStatus: String {
    case orderAccepted
    case orderStarted
    case reportCreated
    case online
    case offline
}
