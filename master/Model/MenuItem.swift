//
//  MenuItem.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 09.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
import UIKit

struct MenuItem {
    var icon: UIImage
    var label: String
}
