//
//  Order.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 25.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
import Firebase

struct Address {
    let city: String?
    let flat: String?
    let house: String?
    let region: String?
    let street: String?
    let building: String?

    var dictionary: [String: Any?] {
        return [
            "city": city,
            "flat": flat,
            "house": house,
            "region": region,
            "street": street,
            "building": building
        ]
    }
    var addressDescriptor: String {
        return "\(city ?? ""), \(street ?? ""), \(house ?? ""), \(building ?? ""),"
    }
    var fullAddressDescriptor: String {
        return "\(city ?? ""), \(street ?? ""), \(house ?? ""), \(building ?? ""), \(flat ?? "")"
    }
}
enum StatusOfOrder: String {
    case sent
    case accepted
    case processing
    case awaiting
    case modernization
    case def
}
enum NotificationState: String {
    case scheduled
    case unnotified
    case notified
}

struct Order {
    let id: String?
    let clientName: String?
    let address: Address?
    let description: String?
    let expireTime: Date?
    let number: String?
    let startWorkTime: Date?
    let status: StatusOfOrder?
    let userId: String?
    let contactPerson: String?
    let timerWasLaunched: Date?
    let problemWithSW: Bool?
    let suspicionForHD: Bool?
    let type: String?
    let clientPhone: String?
    let relatedReportsIds: [String]?
    let canCall: Bool?
    let notificationState: NotificationState?
    let orderWasSeen: Date?

    var dictionary: [String: Any?] {
        return [
            "id": id,
            "clientName": clientName,
            "address": address?.dictionary,
            "description": description,
            "expireTime": expireTime,
            "number": number,
            "startWorkTime": startWorkTime,
            "status": status?.rawValue,
            "userId": userId,
            "contactPerson": contactPerson,
            "timerWasLaunched": timerWasLaunched,
            "problemWithSW":problemWithSW,
            "suspicionForHD":suspicionForHD,
            "type":type,
            "clientPhone":clientPhone,
            "relatedReportsIds": relatedReportsIds,
            "canCall":canCall,
            "notificationState":notificationState,
            "orderWasSeen":orderWasSeen
        ]
    }
    var dictionaryOfRelatedReportsIds: [String]? {
        var dictReports:[String]? = []
        if let reports = dictReports {
            for item in reports {
                dictReports?.append(item)
            }
            //        } else {
            //            dictDetails = nil
        }
        return dictReports
    }

}
extension Address: DocumentSerializable {
    init?(dictionary: [String : Any?], id: String? = nil) {
        let city = dictionary["city"] as? String
        let flat = dictionary["flat"] as? String
        let house = dictionary["house"] as? String
        let region = dictionary["region"] as? String
        let street = dictionary["street"] as? String
        let building = dictionary["building"] as? String

        self.init(city:city,
                  flat:flat,
                  house:house,
                  region:region,
                  street:street,
                  building:building)
    }
}

extension Order: DocumentSerializable {

    init?(dictionary: [String : Any?], id: String?) {
        let id = id

        let dictionaryOfAddress = dictionary["address"] as? [String : Any?] ?? dictionary
        let address = Address(dictionary: dictionaryOfAddress)
        let description = dictionary["description"] as? String
        let number = dictionary["number"] as? String
        let clientName = dictionary["clientName"] as? String
        let statusToEnum = dictionary["status"] as? String ?? "def"
        let status = StatusOfOrder(rawValue: statusToEnum)
        let userId = dictionary["userId"] as? String
        let contactPerson = dictionary["contactPerson"] as? String
        let timestamp1 = dictionary["expireTime"] as? Timestamp
        let expireTime = timestamp1?.dateValue()
        let timestamp2 = dictionary["startWorkTime"] as? Timestamp
        let startWorkTime = timestamp2?.dateValue()
        let timestamp3 = dictionary["timerWasLaunched"] as? Timestamp
        let timerWasLaunched = timestamp3?.dateValue()
        let problemWithSW = dictionary["problemWithSW"] as? Bool
        let suspicionForHD = dictionary["suspicionForHD"] as? Bool
        let type = dictionary["type"] as? String
        let clientPhone = dictionary["clientPhone"] as? String
        let relatedReportsIds = dictionary["relatedReportsIds"] as? [String]
        let canCall = dictionary["canCall"] as? Bool
        let notificationStateEnum = dictionary["notificationState"] as? String
        let notificationState = NotificationState(rawValue: notificationStateEnum ?? "scheduled")
        let timestamp4 = dictionary["orderWasSeen"] as? Timestamp
        let orderWasSeen = timestamp4?.dateValue()

        self.init(id: id,
                  clientName: clientName,
                  address: address,
                  description: description,
                  expireTime: expireTime,
                  number: number,
                  startWorkTime: startWorkTime,
                  status: status,
                  userId: userId,
                  contactPerson: contactPerson,
                  timerWasLaunched: timerWasLaunched,
                  problemWithSW: problemWithSW,
                  suspicionForHD: suspicionForHD,
                  type: type,
                  clientPhone: clientPhone,
                  relatedReportsIds: relatedReportsIds,
                  canCall:canCall,
                  notificationState:notificationState,
                  orderWasSeen:orderWasSeen)
    }
}
