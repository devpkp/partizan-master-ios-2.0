//
//  Report.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 07.08.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation
import Firebase

struct UsedDetails {
    let name: String?
    let price: Double?
    let quantity: Int?

    var dictionary: [String: Any?] {
        return [
            "name": name,
            "price": price,
            "quantity": quantity
        ]
    }
    var list: String {
        return "\(String(describing: name ?? "")): \(String(describing: quantity ?? 0)) шт. \(String(describing: price ?? 0.0)) р. \n"
    }
}

extension UsedDetails: DocumentSerializable {

    init?(dictionary: [String : Any?], id: String? = nil) {
        let name = dictionary["name"] as? String
        let price = dictionary["price"] as? Double
        let quantity = dictionary["quantity"] as? Int

        self.init(name:name,
                  price:price,
                  quantity:quantity)
    }
}
enum StatusOfReport: String {
    case completed
    case sd
    case canceled
    case declined
    case modernization
    case def
}
struct Report {

    let bsoNumber: String?
    let comments: String?
    let cost: Double?
    let agreementDate: Date?
    let defects: String?
    let agreementNumber: String?
    let orderId: String?
    let reportIsAccepted: Bool?
    let status: StatusOfReport?
    let timeSpend: Int?
    let productType: String?
    let usedDetails: [UsedDetails]?
    let userId: String?
    let cancelOrDeclineReason: String?
    let clientName: String?
    let userName: String?

    let orderNumber: String?
    let orderDescription: String?
    let orderAddress: Address?
    let date: Date?

    var dictionary: [String: Any?] {
        return [
            "bsoNumber": bsoNumber,
            "comments": comments,
            "cost": cost,
            "agreementDate": agreementDate,
            "defects": defects,
            "agreementNumber": agreementNumber,
            "orderId": orderId,
            "reportIsAccepted": reportIsAccepted,
            "status": status?.rawValue,
            "timeSpend": timeSpend,
            "productType": productType,
            "usedDetails": dictionaryOfUsedDetails,
            "userId": userId,
            "cancelOrDeclineReason": cancelOrDeclineReason,
            "clientName": clientName,
            "userName":userName,
            "orderNumber": orderNumber,
            "orderDescription": orderDescription,
            "orderAddress": orderAddress?.dictionary,
            "date": date
        ]
    }
    var dictionaryOfUsedDetails: [[String: Any?]]? {
        var dictDetails:[[String: Any?]]? = []
        if let details = usedDetails {
            for item in details {
                dictDetails?.append(item.dictionary)
            }
//        } else {
//            dictDetails = nil
        }
        return dictDetails
    }

}
extension Report: DocumentSerializable {
    init?(dictionary: [String : Any?], id: String? = nil) {
        let bsoNumber = dictionary["bsoNumber"] as? String
        let comments = dictionary["comments"] as? String
        let cost = dictionary["cost"] as? Double
        let agreementDate = dictionary["agreementDate"] as? Date
        let defects = dictionary["defects"] as? String
        let agreementNumber = dictionary["agreementNumber"] as? String
        let orderId = dictionary["orderId"] as? String
        let reportIsAccepted = dictionary["reportIsAccepted"] as? Bool
        let statusToEnum = dictionary["status"] as? String ?? "def"
        let status = StatusOfReport(rawValue: statusToEnum)
        let timeSpend = dictionary["timeSpend"] as? Int
        let productType = dictionary["productType"] as? String
        let cancelOrDeclineReason = dictionary["cancelOrDeclineReason"] as? String

        let usedDetailsDictionary = dictionary["usedDetails"] as? [[String : Any?]] ?? [dictionary]
        
        let usedDetails = usedDetailsDictionary.compactMap {
            return UsedDetails(dictionary: $0)
        }

        let userId = dictionary["userId"] as? String

        let clientName = dictionary["clientName"] as? String
        let userName = dictionary["userName"] as? String

        let orderNumber = dictionary["orderNumber"] as? String
        let orderDescription = dictionary["orderDescription"] as? String
//        let orderAddress = dictionary["orderAddress"] as? String
        let dictionaryOfAddress = dictionary["orderAddress"] as? [String : Any?] ?? dictionary
        let orderAddress = Address(dictionary: dictionaryOfAddress)
        let timestamp = dictionary["date"] as? Timestamp
        let date = timestamp?.dateValue()

        self.init(bsoNumber: bsoNumber,
                  comments: comments,
                  cost: cost,
                  agreementDate: agreementDate,
                  defects: defects,
                  agreementNumber: agreementNumber,
                  orderId: orderId,
                  reportIsAccepted: reportIsAccepted,
                  status: status,
                  timeSpend: timeSpend,
                  productType: productType,
                  usedDetails: usedDetails,
                  userId: userId,
                  cancelOrDeclineReason: cancelOrDeclineReason,
                  clientName: clientName,
                  userName:userName,
                  orderNumber: orderNumber,
                  orderDescription: orderDescription,
                  orderAddress: orderAddress,
                  date: date)
    }
}
