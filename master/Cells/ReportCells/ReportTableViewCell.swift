//
//  ReportTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 17.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {

    @IBOutlet weak var numberLable: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var stateIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
