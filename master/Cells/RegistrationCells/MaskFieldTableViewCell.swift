//
//  MaskFieldTableViewCell.swift
//  master
//
//  Created by Polina Guryeva on 24/10/2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import JMMaskTextField_Swift

class MaskFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var maskField: JMMaskTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
