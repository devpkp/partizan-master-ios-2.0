//
//  RegistrationCheckBoxTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 13.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

class CheckBoxTableViewCell: UITableViewCell {

    @IBOutlet weak var skillSwitch: UISwitch!
    @IBOutlet weak var pointLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

}
