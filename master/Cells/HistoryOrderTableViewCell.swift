//
//  HistoryOrderTableViewCell.swift
//  master
//
//  Created by Polina Guryeva on 13.07.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class HistoryOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOfClient: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var nameOfMaster: UILabel!
    @IBOutlet weak var summa: UILabel!

    @IBOutlet weak var commentOfClient: UITextView!
    @IBOutlet weak var commentOfMaster: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
