//
//  statTableViewCell.swift
//  master
//
//  Created by Polina Guryeva on 12.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class StatTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var line: UIView!
    
    func initStatCell(textColor: UIColor?, countTextColor: UIColor?, text: String?, lineIsVisible: Bool, textSize: CGFloat?) {
        label.textColor = textColor
        countLabel.textColor = countTextColor
        countLabel.text = text
        line.isHidden = !lineIsVisible
        if let size = textSize {
            label.font = UIFont.boldSystemFont(ofSize: size)
        } else {
            label.font = UIFont.systemFont(ofSize: 17)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        label.text = ""
        countLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        countLabel.text = ""
        line.isHidden = false
        label.font = UIFont.systemFont(ofSize: 17)
    }

}
