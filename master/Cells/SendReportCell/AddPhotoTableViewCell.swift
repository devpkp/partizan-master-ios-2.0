//
//  AddPhotoTableViewCell.swift
//  master
//
//  Created by Polina Guryeva on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit
import Photos

class AddPhotoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    weak var viewController: UIViewController?
    weak var navigator: UINavigationController!
    var items = [#imageLiteral(resourceName: "plus")]
    var type: String = ""
    var names: [String] = [""]
    
  
    var activityIndicator = UIActivityIndicatorView(style: .gray)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func commonInit(viewController: UIViewController, navigator: UINavigationController, type: String) {
        self.viewController = viewController
        self.navigator = navigator
        self.type = type
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController.shared
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            viewController?.present(myPickerController, animated: true, completion: nil)
        }
    }

    
    func createPhotoName(image: UIImage) -> String {
        return "\(type)_\(image.hashValue)"
    }
    
    func addActivityIndicator() {
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
        activityIndicator.center = self.contentView.center
    }
    
    func removeActivityIndicator() {
        activityIndicator.removeFromSuperview()
        activityIndicator.stopAnimating()
    }
    deinit {
        print("[AddPhotoTableViewCell] deinit")
    }
}

extension AddPhotoTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? PhotoCollectionViewCell else {return UICollectionViewCell()}
        cell.delegate = self
        cell.collectionView = collectionView
        cell.photo.setImage(items[indexPath.row], for: .normal)
        return cell
    }
}

extension AddPhotoTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 64, height: 64)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    

}

extension AddPhotoTableViewCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            var compressedImage = image.scale()
            let fullName = createPhotoName(image: compressedImage)
            ImageStore.store(image: compressedImage, name: fullName)
        
            names.insert(fullName, at: 1)
            compressedImage = compressedImage.resizeImage(targetSize: CGSize(width: 64, height: 64))
            items.insert(compressedImage, at: 1)
            collectionView.insertItems(at: [IndexPath(row: 1, section: 0)])
            collectionView.reloadData()
        } else {
            print("error")
        }
        viewController?.dismiss(animated: true, completion: nil)
    }
}

extension AddPhotoTableViewCell: PhotoCellDelegate {
    
    func toUIImage(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var image = UIImage()
        option.isSynchronous = true
        option.isNetworkAccessAllowed = true
        manager.requestImage(for: asset, targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight), contentMode: .aspectFit, options: option, resultHandler: { (result, info) -> Void in
            if let res = result {
                image = res
            } else {
                image = UIImage(named: "picture") ?? UIImage()
            }
        })
        return image
    }
    
    func showPhotoLibraryPicker() {
        //   let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let alert = UIAlertController(style: .actionSheet, source: viewController?.view, title: "", message: "", tintColor: #colorLiteral(red: 0.09928072244, green: 0.1677491665, blue: 0.221868366, alpha: 1))
        alert.addPhotoLibraryPicker(flow: .vertical, selectedPhotos: items.count, paging: false, selection: .multiple(action: { [weak self] (assets) in
            for asset in assets {
                let image = self?.toUIImage(asset: asset)
                guard var compressedImage = image?.scale() else {
                    print("[showPhotoLibraryPicker] no compressed photos")
                    return
                }
                let fullName = self?.createPhotoName(image: compressedImage)
                ImageStore.store(image: compressedImage, name: fullName ?? "PhotoName")
                
                self?.names.insert(fullName ?? "PhotoName", at: 1)
                compressedImage = compressedImage.resizeImage(targetSize: CGSize(width: 64, height: 64))
                self?.items.insert(compressedImage, at: 1)
                self?.collectionView.insertItems(at: [IndexPath(row: 1, section: 0)])
            }
            self?.removeActivityIndicator()
            self?.collectionView.reloadData()
            self?.removeActivityIndicator()
        }))
        alert.addAction(title: "Cancel", style: .cancel) { [weak self] _ in
            self?.removeActivityIndicator()
            
        }
        alert.show()
    }
    
    func cellButtonTapped(_ collectionView: UICollectionView, _ indexPath: IndexPath) {
        if indexPath.row == 0 {
            guard let viewControllerAlert = viewController else {
                print("[cellButtonTapped] no viewController")
                return
            }
            guard names.count < 11 else {
                ShowAlerts.showAlert(viewControllerToShow: viewControllerAlert, headerTitle: Constants.errorAlertMessage, message: Constants.tooLongPhotos, okTitle: Constants.okString)
                return
            }
            let alert = UIAlertController(title: Constants.addPhoto, message: nil, preferredStyle: .actionSheet)
            alert.popoverPresentationController?.sourceView = viewController?.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            guard let vc = viewController else {
                print("[AddPhotoTableViewCell] No one ViewControllers")
                return
            }
            alert.popoverPresentationController?.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)
            
            alert.addAction(UIAlertAction(title: Constants.makePhoto, style: .default, handler: {  [weak self] (_) in
                self?.camera()
            }))
            alert.addAction(UIAlertAction(title: Constants.loadFromLibrary, style: .default, handler: { [weak self]  (_) in
                self?.addActivityIndicator()
                self?.showPhotoLibraryPicker()
                
            }))
            alert.addAction(UIAlertAction(title: Constants.cancel, style: .cancel, handler: nil))
            viewController?.present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.popoverPresentationController?.sourceView = viewController?.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            guard let vc = viewController else {
                print("[AddPhotoTableViewCell] No one ViewControllers")
                return
            }
            alert.popoverPresentationController?.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)
            
            alert.addAction(UIAlertAction(title: Constants.showPhoto, style: .default, handler: {  [weak self] (_) in
                guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fullscreenPhotoVC") as? FullscreenPhotoViewController else {return}
                guard (collectionView.cellForItem(at: indexPath) as? PhotoCollectionViewCell) != nil else {return}
                guard let name = self?.names[indexPath.row] else {
                    print("[AddPhotoTableViewCell] no name")
                    return
                }
                vc.image = ImageStore.retrieve(imageNamed: name)
                self?.navigator.pushViewController(vc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: Constants.delete, style: .destructive, handler: {  [weak self] (_) in
                self?.items.remove(at: indexPath.row)
                guard let name = self?.names[indexPath.row] else {
                    print("[AddPhotoTableViewCell] no name")
                    return
                }
                ImageStore.delete(imageNamed: name)
                self?.names.remove(at: indexPath.row)
                self?.collectionView.deleteItems(at: [indexPath])
                self?.collectionView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: Constants.cancel, style: .cancel, handler: nil))
            viewController?.present(alert, animated: true)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

extension UIImagePickerController {
    static let shared = UIImagePickerController()
}
