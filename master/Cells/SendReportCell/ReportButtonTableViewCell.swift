//
//  ReportButtonTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 23.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

protocol SendButtonDelegate: class {
    func didButtonTapped(_ tableView: UITableView, _ indexPath: IndexPath)
}

class ReportButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var button: UIButton!
    weak var tableView: UITableView!
    weak var delegate: SendButtonDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func didTappedButton (_ sender: UIButton) {
        guard let indexPath = tableView.indexPath(for: self) else {return}
        delegate?.didButtonTapped(tableView, indexPath)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        button.setTitle("", for: .normal)
    }

}
