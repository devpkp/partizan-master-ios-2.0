//
//  DetailTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 23.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

protocol DetailCellDelegate: class {
    func didTapDeleteItem(_ tableView: UITableView, _ indexPath: IndexPath)
}

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var detailNameTextField: UITextField!
    @IBOutlet weak var countDetailTextField: UITextField!
    @IBOutlet weak var detailPriceTextField: UITextField!

    weak var tableView: UITableView!
    weak var delegate: DetailCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        guard let indexPath = tableView.indexPath(for: self) else {return}
        delegate?.didTapDeleteItem(tableView, indexPath)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        detailNameTextField.text = ""
        countDetailTextField.text = ""
        detailPriceTextField.text = ""
    }

}
