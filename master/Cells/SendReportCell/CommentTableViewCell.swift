//
//  CommentTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 23.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

protocol AutoFillDelegate: class {
    func didTappedAutoFill(_ tableView: UITableView, _ indexPath: IndexPath)
}

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var comment: UITextView!
    @IBOutlet weak var autoFillingButton: UIButton!
    weak var tableView: UITableView!
    weak var delegate: AutoFillDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func didButtonTapped(_ sender: Any) {
        guard let indexPath = tableView.indexPath(for: self) else {return}
        delegate?.didTappedAutoFill(tableView, indexPath)
    }
    
}
