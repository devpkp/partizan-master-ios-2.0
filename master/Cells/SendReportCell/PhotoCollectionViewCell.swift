//
//  PhotoCollectionViewCell.swift
//  master
//
//  Created by Polina Guryeva on 18.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

protocol PhotoCellDelegate: class {
    func cellButtonTapped(_ collectionView: UICollectionView, _ indexPath: IndexPath)
}

class PhotoCollectionViewCell: UICollectionViewCell {

    weak var delegate: PhotoCellDelegate?
    var collectionView: UICollectionView!
    @IBOutlet weak var photo: UIButton!

    @IBAction func buttonTapped(_ sender: UIButton) {
        guard let indexPath = collectionView.indexPath(for: self) else {return}
        delegate?.cellButtonTapped(collectionView, indexPath)
    }
}
