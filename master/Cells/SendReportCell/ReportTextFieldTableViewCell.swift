//
//  TextFieldTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 23.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

class ReportTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        textField.inputView = nil
        textField.inputAccessoryView = nil
        textField.keyboardType = .default
        textField.placeholder = ""
        textField.text = ""
    }
}
