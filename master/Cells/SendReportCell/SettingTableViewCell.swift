//
//  settingTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 18.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

class ReportTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var settingIcon: UIImageView!
    @IBOutlet weak var settingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
