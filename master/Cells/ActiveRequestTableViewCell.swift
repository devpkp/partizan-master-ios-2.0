//
//  ActiveRequestTableViewCell.swift
//  master
//
//  Created by Polina Guryeva on 03.09.2018.
//  Copyright © 2018 Sergey Gusev. All rights reserved.
//

import UIKit

class ActiveRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOfClient: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var sendReportButton: UIButton!
    @IBOutlet weak var isModernizationLabel: UILabel!
    
    @IBOutlet weak var frameView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        frameView.layer.shadowColor = UIColor.lightGray.cgColor
        frameView.layer.shadowOpacity = 1.0
        frameView.layer.shadowOffset = .zero
        frameView.layer.shadowRadius = 3.0
        frameView.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
