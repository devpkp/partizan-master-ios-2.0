//
//  PersonalDataTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 31.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

class PersonalDataTableViewCell: UITableViewCell {

    @IBOutlet weak var secondName: UILabel!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var patronymic: UILabel!
    @IBOutlet weak var id: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
