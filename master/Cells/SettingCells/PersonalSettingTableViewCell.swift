//
//  PersonalSettingTableViewCell.swift
//  partizanMaster
//
//  Created by Polina Guryeva on 31.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import UIKit

class PersonalSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var soundSwitch: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
