//
//  Constants.swift
//  partizanMaster
//
//  Created by Sergey Gusev on 17.07.2018.
//  Copyright © 2018 Иван Кулчеев. All rights reserved.
//

import Foundation

class Constants {
    static let errorAuth = "Ошибка авторизации"
    static let enterCorrectData = "Введите корректные данные"
    static let okString = "Хорошо"

    static let stopTimer = "Время закончилось"

    static let forbiddenSymbols = "¿¡«»‹›‘’“”‚„§¶•—–…′″∞´¨˜‾¦±~`&$%'\\\"\\'<>{}[]|§"
    
    static let errorAlertMessage = "Ошибка"
    static let inputForbiddenSymbols = "Вы ввели неккоректный символ"
    
    static let attensionAlertMessage = "Внимание"
    static let areYouSureToSendReport = "Вы уверены, что хотите отправить отчет?"
    static let cancel = "Отмена"
    static let send = "Отправить"
    static let yes = "Да"
    static let no = "Нет"
    static let passwordsIsNotSimilar = "Пароли не совпадают"
    static let enterAllFields = "Введите все поля"
    static let completeRegistration =  "Регистрация прошла успешно"
    
    static let addPhoto = "Добавить фото"
    static let makePhoto = "Сфотографировать"
    static let loadFromLibrary = "Загрузить из галереи"
    static let delete = "Удалить фото"
    static let showPhoto =  "Посмотреть фото"
    
    static let fromWork = "Вы уверены, что хотите уйти со смены?"
    static let toWork = "Вы уверены, что хотите выйти на смену?"
    static let youCanNotGoAway = "Вы не можете уйти со смены с активной заявкой"
    static let somethingWrong = "Что-то пошло не так"
    
    static let tryAgain = "Попробуйте еще"
    static let canNotLoadPhoto = "Не удалось загрузить фото"
    
    static let canNotShowPath = "Не получилось отобразить точку на карте"
    static let chooseApp = "Выберите приложение для навигации"
    static let chooseReason = "Выберите причину отказа"
    
    static let areYouSureAcceptOrder = "Вы уверены, что хотите принять заявку?"
    static let areYouSureStartWork = "Вы уверены, что хотите начать работу?"
    static let areYouSureToRefuseOrder = "Вы уверены, что хотите отказаться от заявки?"
    
    static let enterCorrectInterval = "Введите корректный интервал для суммы (от, до)"
    static let pleaseWait = "Подождите, пожалуйста"
    static let loadingFiles = "Идет загрузка..."
    
    static let anotherAuth = "Возможно кто-то произвел вход в ваш аккаунт"
    
    static let checkCurrentOrder = "Вы не можете изменить статус заявки\nПроверьте соединение"
    
    static let checkOnlineStatus = "Вы не можете изменять статус заявки в режиме\"Offline\""
    
    
    static let accept = "Напоминаем о начале выполнения заявки через один час"
    
    static let tooLongCount = "Допустимое количество символов - 254\nВы ввели слижком много символов в поле \"Комментарий\" "
    static let tooLongInt = "Количество знаков в цифровых полях более 10"
    static let tooLongCost = "Количество знаков в денежных полях не может превышать 7 цифр"
    static let tooLongPhotos = "Количество фотографий в каждой категории не должно превышать 10 штук"
}
